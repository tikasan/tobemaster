<?php

/**
 * Created by PhpStorm.
 * User: hiroaki
 * Date: 2017/06/12
 * Time: 13:32
 */
namespace Controllers;

use Illuminate\Database\QueryException;
use Models\User;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\UploadedFileInterface;

class ManagementController {
	protected $container;
	const PAGE_SIZE = 20;
	const DOCUMENT_ROOT = __DIR__.'/../../public';
	const FILE_SAVE_PATH = '/tmp/';

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	/**
	 * ログイン画面
	 * @param $request
	 * @param $response
	 * @param $args
	 */
	public function index($request, $response, $args) {
		return $this->container->view->render($response, 'login.tpl');
	}

	public function login($request, $response, $args) {
		$parsedBody = $request->getParsedBody();

		$userId = @$parsedBody['user_id'];
		$password = @$parsedBody['password'];
		$user = $this->container['db']->table('m_user')->where('user_id', $userId)->where('password', $password)->first();
		if (!empty($user)) {
			\Models\LoginManager::set($user->name, $user->user_id);
			return $response->withStatus(200)->withHeader('Location', '/admin/questions');
		}
		return $this->container->view->render($response, 'login.tpl', [
			'error_message' => 'ログインIDまたはパスワードが間違っています',
			'userId' => $userId
		]);
	}

	public function logout($request, $response, $args) {
		\Models\LoginManager::delete();
		return $response->withStatus(200)->withHeader('Location', '/admin');
	}

	public function getQuestions($request, $response, $args) {
		$parsedBody = $request->getQueryParams();

		$title = @$parsedBody['title'];
		$level = @$parsedBody['level'];
		$area = @$parsedBody['area'];
		$page = @$parsedBody['page'];
		if (empty($page)) $page = 1;


		$questions = $this->container['db']->table('m_question');
		if (!empty($title)) $questions = $questions->where('title', 'LIKE', '%' . $title . '%');
		if (isset($level) && ($level === '0' || $level === '1')) $questions = $questions->where('is_easy', $level);
		if (!empty($area)) $questions = $questions->where('area', $area);
		$max = $questions->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		if (!empty($page) && $page > 0) $questions = $questions->skip(self::PAGE_SIZE * ($page - 1))->take(self::PAGE_SIZE);
		else $questions = $questions->take(self::PAGE_SIZE);
		$questions = $questions->get();

		if ($page > $pageCount) $page = 1;
		if ($page > 2) $start = $page - 2;
		elseif ($page > 1) $start = $page - 1;
		else $start = $page;

		if (!isset($level) || ($level !== '0' && $level !== '1')) $level = 2;
		return $this->container->view->render($response, 'questions.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'questionsList' => $questions,
			'title' => $title,
			'level' => $level,
			'area' => $area,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}

	public function addQuestion($request, $response, $args) {
		$templates = $this->container['db']->table('m_template')->get();

		return $this->container->view->render($response, 'questions_add.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'area' => '',
			'templateList' => $templates
		]);
	}

	private function getImageExtension($imageTmpUrl) {
		$tmp_size = getimagesize($imageTmpUrl); // 一時ファイルの情報を取得
		$img = $extension = null;
		switch ($tmp_size[2]) { // 画像の種類を判別
			case 1 : // GIF
				$img = imagecreatefromgif($imageTmpUrl);
				$extension = '.gif';
				break;
			case 2 : // JPEG
				$img = imagecreatefromjpeg($imageTmpUrl);
				$extension = '.jpg';
				break;
			case 3 : // PNG
				$img = imagecreatefrompng($imageTmpUrl);
				$extension = '.png';
				break;
			default : break;
		}
		return [
			'img' => $img,
			'extension' => $extension
		];
	}

	// 画像サイズを変更する関数
	function getResizeImageSize($img = null, $maxsizeW = 300, $maxsizeH = 460) {
		if (!$img) return false;
		$w0 = $w1 = imagesx($img); // 画像リソースの幅
		$h0 = $h1 = imagesy($img); // 画像リソースの高さ
		if ($w0 > $maxsizeW) { // $maxsize以下の大きさに変更する
			$w1 = $maxsizeW;
			$h1 = (int) $h0 * ($maxsizeW / $w0);
		}
		if ($h1 > $maxsizeH) {
			$w1 = (int) $w1 * ($maxsizeH / $h1);
			$h1 = $maxsizeH;
		}

		return [
			'w0'=>$w0, // 元画像の幅
			'h0'=>$h0, // 元画像の高さ
			'w1'=>$w1, // 保存画像の幅
			'h1'=>$h1, // 保存画像の高さ
		];
	}

	// 画像を保存する関数
	function saveImage($img = null, $file = null, $ext = null) {
		if (!$img || !$file || !$ext) return false;
		switch ($ext) {
			case ".jpg" :
				$result = imagejpeg($img, $file);
				break;
			case ".gif" :
				$result = imagegif($img, $file);
				break;
			case ".png" :
				$result = imagepng($img, $file);
				break;
			default : return false; break;
		}
		chmod($file, 0644); // パーミッション変更
		return $result;
	}

	public function addQuestionExecute($request, $response, $args) {
		$parsedBody = $request->getParsedBody();
		$uploaded_files = $request->getUploadedFiles();
		$templates = $this->container['db']->table('m_template')->get();

		$title = @$parsedBody['title'];
		$image1 = @$uploaded_files['image1'];
		$imageUrl1 = @$parsedBody['imageUrl1'];
		$image2 = @$uploaded_files['image2'];
		$imageUrl2 = @$parsedBody['imageUrl2'];
		$area = @$parsedBody['area'];
		$isEasy = @$parsedBody['isEasy'];
		$isCorrect = @$parsedBody['isCorrect'];
		$comment = @$parsedBody['comment'];

		$errorMessage = [];
		$imagePath1 = '';
		$imagePath2 = '';
		// 空白チェック
		if (empty($title)) $errorMessage[] = '問題文が入力されていません。';
		if (!empty($image1->file)) {
			$imageTmpUrl1 = $image1->file;
			$image1Info = $this->getImageExtension($imageTmpUrl1);
			$imagePath1 = self::FILE_SAVE_PATH.hash_file('md5', $imageTmpUrl1).$image1Info['extension'];
			$img_size = $this->getResizeImageSize($image1Info['img']);
			$out = imagecreatetruecolor($img_size['w1'], $img_size['h1']); // 新しい画像データ
			$color_white = imagecolorallocate($out, 255, 255, 255); // 色データを作成
			imagefill($out, 0, 0, $color_white);
			// $imgの画像情報を$outにコピーする
			imagecopyresampled(
				$out, // コピー先
				$image1Info['img'], // コピー元
				0, 0, 0, 0, // 座標(コピー先:x, コピー先:y, コピー元:x, コピー元:y)
				$img_size['w1'], $img_size['h1'], $img_size['w0'], $img_size['h0'] // サイズ(コピー先:幅, コピー先:高さ, コピー元:幅, コピー元:高さ)
			);
			if (!$this->saveImage($out, self::DOCUMENT_ROOT.$imagePath1, $image1Info['extension'])) {
				$imagePath1 = '';
				$errorMessage[] = '画像1を保存できませんでした。';
			};
			imagedestroy($out);
			imagedestroy($image1Info['img']);
		} else if (!empty($imageUrl1)) $imagePath1 = $imageUrl1;
		else $errorMessage[] = '画像1が選択されていません。';
		if (!empty($image2->file)) {
			$imageTmpUrl2 = $image2->file;
			$image2Info = $this->getImageExtension($imageTmpUrl2);
			$imagePath2 = self::FILE_SAVE_PATH.hash_file('md5', $imageTmpUrl2).$image2Info['extension'];
			$img_size = $this->getResizeImageSize($image2Info['img']);
			$out = imagecreatetruecolor($img_size['w1'], $img_size['h1']); // 新しい画像データ
			$color_white = imagecolorallocate($out, 255, 255, 255); // 色データを作成
			imagefill($out, 0, 0, $color_white);
			// $imgの画像情報を$outにコピーする
			imagecopyresampled(
				$out, // コピー先
				$image2Info['img'], // コピー元
				0, 0, 0, 0, // 座標(コピー先:x, コピー先:y, コピー元:x, コピー元:y)
				$img_size['w1'], $img_size['h1'], $img_size['w0'], $img_size['h0'] // サイズ(コピー先:幅, コピー先:高さ, コピー元:幅, コピー元:高さ)
			);
			if (!$this->saveImage($out, self::DOCUMENT_ROOT.$imagePath2, $image2Info['extension'])) {
				$imagePath2 = '';
				$errorMessage[] = '画像2を保存できませんでした。';
			};
			imagedestroy($out);
			imagedestroy($image2Info['img']);
		} else if (!empty($imageUrl2)) $imagePath2 = $imageUrl2;
		else $errorMessage[] = '画像2が選択されていません。';
		if (empty($area)) $errorMessage[] = 'エリアが選択されていません。';
		if (!isset($isCorrect) || ($isCorrect !== '1' && $isCorrect !== '0')) $errorMessage[] = '解答が選択されていません。';
		if (!isset($isEasy) || ($isEasy !== '1' && $isEasy !== '0')) $errorMessage[] = '難易度が設定されていません。';
		if (!empty($errorMessage)) {
			return $this->container->view->render($response, 'questions_add.tpl', [
				'error_message' => $errorMessage,
				'user_name' => \Models\LoginManager::getUserName(),
				'title' => $title,
				'imageUrl1' => $imagePath1,
				'imageUrl2' => $imagePath2,
				'area' => $area,
				'isEasy' => $isEasy,
				'isCorrect' => $isCorrect,
				'comment' => $comment,
				'templateList' => $templates
			]);
		}

		try {
			$result = $this->container['db']->table('m_question')->insert([
				'title' => $title,
				'image_url_1' => $imagePath1,
				'image_url_2' => $imagePath2,
				'is_easy' => $isEasy,
				'is_correct' => $isCorrect,
				'area' => $area,
				'comment' => $comment
			]);
		} catch (QueryException $e) {
			$error_message[] = '登録できませんでした。';
			return $this->container->view->render($response, 'questions_add.tpl', [
				'error_message' => $errorMessage,
				'user_name' => \Models\LoginManager::getUserName(),
				'title' => $title,
				'imageUrl1' => $imagePath1,
				'imageUrl2' => $imagePath2,
				'area' => $area,
				'isEasy' => $isEasy,
				'isCorrect' => $isCorrect,
				'comment' => $comment,
				'templateList' => $templates
			]);
		}

		$questions = $this->container['db']->table('m_question');
		$max = $questions->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		$questions = $questions->take(self::PAGE_SIZE)->get();

		return $this->container->view->render($response, 'questions.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'add_message' => '問題を追加しました。',
			'questionsList' => $questions,
			'area' => '',
			'level' => 2,
			'page' => 1,
			'start' => 1,
			'pageCount' => $pageCount
		]);
	}

	public function editQuestion($request, $response, $args) {
		$id = $args['id'];
		$templates = $this->container['db']->table('m_template')->get();
		$question = $this->container['db']->table('m_question')->where('id', $id)->first();

		$error_message = [];
		if (empty($question)) {
			$error_message[] = '存在しない問題が選択されました';
			return $this->container->view->render($response, 'questions_edit_comp.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'templateList' => $templates,
				'id' => $id
			]);
		}

		return $this->container->view->render($response, 'questions_edit_comp.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'templateList' => $templates,
			'id' => $id,
			'title' => $question->title,
			'imageUrl1' => $question->image_url_1,
			'imageUrl2' => $question->image_url_2,
			'area' => $question->area,
			'isEasy' => $question->is_easy,
			'isCorrect' => $question->is_correct,
			'comment' => $question->comment,
		]);
	}

	public function editQuestionExecute($request, $response, $args) {
		$parsedBody = $request->getParsedBody();
		$uploaded_files = $request->getUploadedFiles();
		$templates = $this->container['db']->table('m_template')->get();

		$id = @$parsedBody['id'];
		$title = @$parsedBody['title'];
		$image1 = @$uploaded_files['image1'];
		$imageUrl1 = @$parsedBody['imageUrl1'];
		$image2 = @$uploaded_files['image2'];
		$imageUrl2 = @$parsedBody['imageUrl2'];
		$area = @$parsedBody['area'];
		$isEasy = @$parsedBody['isEasy'];
		$isCorrect = @$parsedBody['isCorrect'];
		$comment = @$parsedBody['comment'];

		$errorMessage = [];
		$imagePath1 = '';
		$imagePath2 = '';
		// 空白チェック
		if (empty($title)) $errorMessage[] = '問題文が入力されていません。';
		if (!empty($image1->file)) {
			$imageTmpUrl1 = $image1->file;
			$image1Info = $this->getImageExtension($imageTmpUrl1);
			$imagePath1 = self::FILE_SAVE_PATH.hash_file('md5', $imageTmpUrl1).$image1Info['extension'];
			$img_size = $this->getResizeImageSize($image1Info['img']);
			$out = imagecreatetruecolor($img_size['w1'], $img_size['h1']); // 新しい画像データ
			$color_white = imagecolorallocate($out, 255, 255, 255); // 色データを作成
			imagefill($out, 0, 0, $color_white);
			// $imgの画像情報を$outにコピーする
			imagecopyresampled(
				$out, // コピー先
				$image1Info['img'], // コピー元
				0, 0, 0, 0, // 座標(コピー先:x, コピー先:y, コピー元:x, コピー元:y)
				$img_size['w1'], $img_size['h1'], $img_size['w0'], $img_size['h0'] // サイズ(コピー先:幅, コピー先:高さ, コピー元:幅, コピー元:高さ)
			);
			if (!$this->saveImage($out, self::DOCUMENT_ROOT.$imagePath1, $image1Info['extension'])) {
				$imagePath1 = '';
				$errorMessage[] = '画像1を保存できませんでした。';
			};
			imagedestroy($out);
			imagedestroy($image1Info['img']);
		} else if (!empty($imageUrl1)) $imagePath1 = $imageUrl1;
		else $errorMessage[] = '画像1が選択されていません。';
		if (!empty($image2->file)) {
			$imageTmpUrl2 = $image2->file;
			$image2Info = $this->getImageExtension($imageTmpUrl2);
			$imagePath2 = self::FILE_SAVE_PATH.hash_file('md5', $imageTmpUrl2).$image2Info['extension'];
			$img_size = $this->getResizeImageSize($image2Info['img']);
			$out = imagecreatetruecolor($img_size['w1'], $img_size['h1']); // 新しい画像データ
			$color_white = imagecolorallocate($out, 255, 255, 255); // 色データを作成
			imagefill($out, 0, 0, $color_white);
			// $imgの画像情報を$outにコピーする
			imagecopyresampled(
				$out, // コピー先
				$image2Info['img'], // コピー元
				0, 0, 0, 0, // 座標(コピー先:x, コピー先:y, コピー元:x, コピー元:y)
				$img_size['w1'], $img_size['h1'], $img_size['w0'], $img_size['h0'] // サイズ(コピー先:幅, コピー先:高さ, コピー元:幅, コピー元:高さ)
			);
			if (!$this->saveImage($out, self::DOCUMENT_ROOT.$imagePath2, $image2Info['extension'])) {
				$imagePath2 = '';
				$errorMessage[] = '画像2を保存できませんでした。';
			};
			imagedestroy($out);
			imagedestroy($image2Info['img']);
		} else if (!empty($imageUrl2)) $imagePath2 = $imageUrl2;
		else $errorMessage[] = '画像2が選択されていません。';
		if (empty($area)) $errorMessage[] = 'エリアが選択されていません。';
		if (!isset($isCorrect) || ($isCorrect !== '1' && $isCorrect !== '0')) $errorMessage[] = '解答が選択されていません。';
		if (!isset($isEasy) || ($isEasy !== '1' && $isEasy !== '0')) $errorMessage[] = '難易度が設定されていません。';
		if (!empty($errorMessage)) {
			return $this->container->view->render($response, 'questions_edit_comp.tpl', [
				'id' => $id,
				'error_message' => $errorMessage,
				'user_name' => \Models\LoginManager::getUserName(),
				'title' => $title,
				'imageUrl1' => $imagePath1,
				'imageUrl2' => $imagePath2,
				'area' => $area,
				'isEasy' => $isEasy,
				'isCorrect' => $isCorrect,
				'comment' => $comment,
				'templateList' => $templates
			]);
		}

		try {
			$result = $this->container['db']->table('m_question')->where('id', $id)->update([
				'title' => $title,
				'image_url_1' => $imagePath1,
				'image_url_2' => $imagePath2,
				'is_easy' => $isEasy,
				'is_correct' => $isCorrect,
				'area' => $area,
				'comment' => $comment
			]);
		} catch (QueryException $e) {
			$error_message[] = '更新できませんでした。';
			return $this->container->view->render($response, 'questions_edit_comp.tpl', [
				'id' => $id,
				'error_message' => $errorMessage,
				'user_name' => \Models\LoginManager::getUserName(),
				'title' => $title,
				'imageUrl1' => $imagePath1,
				'imageUrl2' => $imagePath2,
				'area' => $area,
				'isEasy' => $isEasy,
				'isCorrect' => $isCorrect,
				'comment' => $comment,
				'templateList' => $templates
			]);
		}

		$questions = $this->container['db']->table('m_question');
		$max = $questions->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		$questions = $questions->take(self::PAGE_SIZE)->get();

		return $this->container->view->render($response, 'questions.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'add_message' => '問題を編集しました。',
			'questionsList' => $questions,
			'area' => '',
			'level' => 2,
			'page' => 1,
			'start' => 1,
			'pageCount' => $pageCount
		]);
	}

	public function deleteQuestionExecute($request, $response, $args) {
		$error_message = [];
		$id = $args['id'];

		$questions = $this->container['db']->table('m_question');
		$max = $questions->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		$questions = $questions->take(self::PAGE_SIZE)->get();

		$question = $this->container['db']->table('m_question')->where('id', $id)->first();
		if (empty($question)) {
			$error_message[] = '存在しない問題が選択されました';
			return $this->container->view->render($response, 'questions.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'questionsList' => $questions,
				'page' => 1,
				'start' => 1,
				'pageCount' => $pageCount
			]);
		}

		try {
			$result = $this->container['db']->table('m_question')->where('id', $id)->delete();
		} catch (QueryException $e) {
			$error_message[] = '削除できませんでした。';
			return $this->container->view->render($response, 'questions.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'questionsList' => $questions,
				'page' => 1,
				'start' => 1,
				'pageCount' => $pageCount
			]);
		}

		$questions = $this->container['db']->table('m_question');
		$max = $questions->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		$questions = $questions->take(self::PAGE_SIZE)->get();
		return $this->container->view->render($response, 'questions.tpl', [
			'delete_message' => '削除しました。',
			'user_name' => \Models\LoginManager::getUserName(),
			'questionsList' => $questions,
			'page' => 1,
			'start' => 1,
			'pageCount' => $pageCount
		]);
	}

	public function getUsers($request, $response, $args) {
		$parsedBody = $request->getQueryParams();

		$page = @$parsedBody['page'];
		if (empty($page)) $page = 1;
		$users = $this->container['db']->table('m_user');

		$max = $this->container['db']->table('m_user')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		$errorMessage = '';
		if ($page > $pageCount) {
			$page = 1;
			$errorMessage = "正しいページ番号を指定してください";
		}

		if (!empty($page) && $page > 0) $users = $users->skip(self::PAGE_SIZE * ($page - 1))->take(self::PAGE_SIZE);
		else $users = $users->take(self::PAGE_SIZE);
		$users = $users->get();

		if ($page > 2) $start = $page - 2;
		elseif ($page > 1) $start = $page - 1;
		else $start = $page;

		return $this->container->view->render($response, 'users.tpl', [
			'error_message' => $errorMessage,
			'user_name' => \Models\LoginManager::getUserName(),
			'user_id' => \Models\LoginManager::getUserId(),
			'userList' => $users,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);

	}

	public function addUser($request, $response, $args) {
		return $this->container->view->render($response, 'users_add.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
		]);
	}

	public function addUserExecute($request, $response, $args) {
		$parsedBody = $request->getParsedBody();

		$id = @$parsedBody['id'];
		$name = @$parsedBody['name'];
		$password = @$parsedBody['password'];
		$checkPassword = @$parsedBody['checkPassword'];

		$error_message = [];
		if (empty($id)) $error_message[] = 'ユーザーIDが未入力です。';
		if (empty($name)) $error_message[] = '名前が未入力です。';
		if (empty($password) || ($password !== $checkPassword)) {
			$error_message[] = 'パスワードを確認してください';
		}

		if (!empty($error_message)) {
			return $this->container->view->render($response, 'users_add.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'id' => $id,
				'name' => $name
			]);
		}

		$user = $this->container['db']->table('m_user')->where('user_id', $id)->first();
		if (!empty($user)) {
			$error_message[] = 'そのユーザーIDは登録済みの為、使用できません。';
			return $this->container->view->render($response, 'users_add.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'id' => $id,
				'name' => $name
			]);
		}


		try {
			$result = $this->container['db']->table('m_user')->insert([
				'user_id' => $id,
				'name' => $name,
				'password' => $password
			]);
		} catch (QueryException $e) {
			$error_message[] = '登録できませんでした。';
			return $this->container->view->render($response, 'users_add.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'id' => $id,
				'name' => $name
			]);
		}


		$page = 1;
		$start = 1;
		$users = $this->container['db']->table('m_user');
		$max = $this->container['db']->table('m_user')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		if (!empty($page) && $page > 0) $users = $users->skip(self::PAGE_SIZE * ($page - 1))->take(self::PAGE_SIZE);
		else $users = $users->take(self::PAGE_SIZE);
		$users = $users->get();

		return $this->container->view->render($response, 'users.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'add_message' => 'ユーザーを追加しました。',
			'user_id' => \Models\LoginManager::getUserId(),
			'userList' => $users,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}

	public function editUser($request, $response, $args) {
		$id = $args['id'];
		$user = $this->container['db']->table('m_user')->where('user_id', $id)->first();
		return $this->container->view->render($response, 'users_edit.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'user' => $user
		]);
	}

	public function editUserExecute($request, $response, $args) {
		$error_message = [];

		$page = 1;
		$start = 1;
		$max = $this->container['db']->table('m_user')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		$users = $this->container['db']->table('m_user')->take(self::PAGE_SIZE)->get();

		$id = $args['id'];
		$user = $this->container['db']->table('m_user')->where('user_id', $id)->first();
		if (empty($user)) {
			$error_message[] = '存在しないユーザが選択されました。';
			return $this->container->view->render($response, 'users.tpl', [
				'user_name' => \Models\LoginManager::getUserName(),
				'error_message' => $error_message,
				'user_id' => \Models\LoginManager::getUserId(),
				'userList' => $users,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		$parsedBody = $request->getParsedBody();

		$name = @$parsedBody['name'];
		$password = @$parsedBody['password'];
		$checkPassword = @$parsedBody['password_check'];

		$error_message = [];
		$updateArray = [];
		if (empty($name)) $error_message[] = '名前欄を空にすることはできません';
		else $updateArray['name'] = $name;
		if (($password !== $checkPassword)) {
			$error_message[] = 'パスワードを確認してください';
		} else if (!empty($password)) {
			$updateArray['password'] = $password;
		}

		if ($error_message) {
			return $this->container->view->render($response, 'users_edit.tpl', [
				'error_message' => $error_message,
				'user' => new User($id, $name),
				'user_name' => \Models\LoginManager::getUserName()
			]);
		}

		try {
			$result = $this->container['db']->table('m_user')->where('user_id', $id)->update($updateArray);
		} catch (QueryException $e) {
			$error_message[] = '更新できませんでした。';
			return $this->container->view->render($response, 'users_edit.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'user' => new User($id, $name),
			]);
		}

		$max = $this->container['db']->table('m_user')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		$users = $this->container['db']->table('m_user')->take(self::PAGE_SIZE)->get();

		return $this->container->view->render($response, 'users.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'edit_message' => $id . 'の情報を更新しました。',
			'user_id' => \Models\LoginManager::getUserId(),
			'userList' => $users,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}

	public function deleteUserExecute($request, $response, $args) {
		$error_message = [];

		$page = 1;
		$start = 1;
		$max = $this->container['db']->table('m_user')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		$users = $this->container['db']->table('m_user')->take(self::PAGE_SIZE)->get();

		$id = $args['id'];
		$user = $this->container['db']->table('m_user')->where('user_id', $id)->first();
		if (empty($user)) {
			$error_message[] = '存在しないユーザが選択されました。';
		}

		if (empty($error_message)) {
			try {
				$result = $this->container['db']->table('m_user')->where('user_id', $id)->delete();
			} catch (QueryException $e) {
				$error_message[] = '削除できませんでした。';
			}
		}

		if (!empty($error_message)) {
			return $this->container->view->render($response, 'users.tpl', [
				'user_name' => \Models\LoginManager::getUserName(),
				'error_message' => $error_message,
				'user_id' => \Models\LoginManager::getUserId(),
				'userList' => $users,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		$users = $this->container['db']->table('m_user')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_user')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		return $this->container->view->render($response, 'users.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'delete_message' => $id . 'を削除しました。',
			'user_id' => \Models\LoginManager::getUserId(),
			'userList' => $users,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}

	public function getRanks($request, $response, $args) {
		$parsedBody = $request->getQueryParams();

		$level = @$parsedBody['level'];
		if (empty($level)) $level = 'low';
		$ranks = $this->container['db']->table('m_rank')->where('id', $level)->first();
		return $this->container->view->render($response, 'ranks.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'ranks' => $ranks,
			'level' => $level
		]);
	}

	public function editRanksExecute($request, $response, $args) {
		$parsedBody = $request->getParsedBody();

		$level = $parsedBody['level'];
		$range1 = $parsedBody['range1'];
		$range2 = $parsedBody['range2'];
		$range3 = $parsedBody['range3'];
		$range4 = $parsedBody['range4'];


		if ($range1 >= $range2 || $range2 >= $range3 || $range3 >= $range4 || $range4 > 10) {
			$ranks = $this->container['db']->table('m_rank')->where('id', $level)->first();
			return $this->container->view->render($response, 'ranks.tpl', [
				'error_message' => 'レンジが正しく設定されていません',
				'user_name' => \Models\LoginManager::getUserName(),
				'level' => $level,
				'ranks' => $ranks
			]);
		}

		try {
			$result = $this->container['db']->table('m_rank')->where('id', $level)->update([
				'range_1' => $range1,
				'range_2' => $range2,
				'range_3' => $range3,
				'range_4' => $range4
			]);
		} catch (QueryException $e) {
			$ranks = $this->container['db']->table('m_rank')->where('id', $level)->first();
			return $this->container->view->render($response, 'ranks.tpl', [
				'error_message' => '値を正しく更新できませんでした。',
				'user_name' => \Models\LoginManager::getUserName(),
				'level' => $level,
				'ranks' => $ranks
			]);
		}

		$ranks = $this->container['db']->table('m_rank')->where('id', $level)->first();
		return $this->container->view->render($response, 'ranks.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'message' => 'ランク情報を編集しました。',
			'ranks' => $ranks,
			'level' => $level
		]);
	}

	public function getTemplates($request, $response, $args) {
		$parsedBody = $request->getQueryParams();

		$page = @$parsedBody['page'];
		if (empty($page)) $page = 1;
		$templates = $this->container['db']->table('m_template');

		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		$errorMessage = '';
		if ($page > $pageCount) {
			$page = 1;
			$errorMessage = "正しいページ番号を指定してください";
		}

		if (!empty($page) && $page > 0) $templates = $templates->skip(self::PAGE_SIZE * ($page - 1))->take(self::PAGE_SIZE);
		else $templates = $templates->take(self::PAGE_SIZE);
		$templates = $templates->get();

		if ($page > 2) $start = $page - 2;
		elseif ($page > 1) $start = $page - 1;
		else $start = $page;

		return $this->container->view->render($response, 'template.tpl', [
			'error_message' => $errorMessage,
			'user_name' => \Models\LoginManager::getUserName(),
			'templateList' => $templates,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}

	public function addTemplateExecute($request, $response, $args) {
		$templates = $this->container['db']->table('m_template')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		$parsedBody = $request->getParsedBody();
		$title = $parsedBody['title'];
		if (empty($title)) {
			return $this->container->view->render($response, 'template.tpl', [
				'error_message' => "問題タイトルが入力されていません",
				'user_name' => \Models\LoginManager::getUserName(),
				'templateList' => $templates,
				'page' => 1,
				'start' => 1,
				'pageCount' => $pageCount
			]);
		}

		try {
			$result = $this->container['db']->table('m_template')->insert([
				'title' => $title
			]);
		} catch (QueryException $e) {
			return $this->container->view->render($response, 'template.tpl', [
				'error_message' => "テンプレートを追加できませんでした。",
				'user_name' => \Models\LoginManager::getUserName(),
				'templateList' => $templates,
				'page' => 1,
				'start' => 1,
				'pageCount' => $pageCount
			]);
		}

		$templates = $this->container['db']->table('m_template')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		return $this->container->view->render($response, 'template.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'add_message' => '問題テンプレートを追加しました。',
			'templateList' => $templates,
			'page' => 1,
			'start' => 1,
			'pageCount' => $pageCount
		]);
	}

	public function editTemplateExecute($request, $response, $args) {
		$error_message = [];

		$page = 1;
		$start = 1;
		$templates = $this->container['db']->table('m_template')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		$id = $args['id'];
		$template = $this->container['db']->table('m_template')->where('id', $id)->first();
		if (empty($template)) {
			$error_message[] = '存在しないテンプレートが選択されました。';
			return $this->container->view->render($response, 'template.tpl', [
				'user_name' => \Models\LoginManager::getUserName(),
				'error_message' => $error_message,
				'templateList' => $templates,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		$parsedBody = $request->getParsedBody();

		$title = @$parsedBody['title'];
		$error_message = [];
		if (empty($title)) $error_message[] = 'テンプレートが入力されていません。';

		if ($error_message) {
			return $this->container->view->render($response, 'template.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'templateList' => $templates,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		try {
			$result = $this->container['db']->table('m_template')->where('id', $id)->update([
				'title' => $title
			]);
		} catch (QueryException $e) {
			$error_message[] = '更新できませんでした。';
			return $this->container->view->render($response, 'template.tpl', [
				'error_message' => $error_message,
				'user_name' => \Models\LoginManager::getUserName(),
				'templateList' => $templates,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		$templates = $this->container['db']->table('m_template')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		return $this->container->view->render($response, 'template.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'edit_message' => 'テンプレート情報を更新しました。',
			'user_id' => \Models\LoginManager::getUserId(),
			'templateList' => $templates,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}

	public function deleteTemplateExecute($request, $response, $args) {
		$error_message = [];

		$page = 1;
		$start = 1;
		$templates = $this->container['db']->table('m_template')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;

		$id = $args['id'];
		$template = $this->container['db']->table('m_template')->where('id', $id)->first();
		if (empty($template)) {
			$error_message[] = '存在しないテンプレートが選択されました。';
			return $this->container->view->render($response, 'template.tpl', [
				'user_name' => \Models\LoginManager::getUserName(),
				'error_message' => $error_message,
				'templateList' => $templates,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		if (empty($error_message)) {
			try {
				$result = $this->container['db']->table('m_template')->where('id', $id)->delete();
			} catch (QueryException $e) {
				$error_message[] = '削除できませんでした。';
			}
		}

		if (!empty($error_message)) {
			return $this->container->view->render($response, 'template.tpl', [
				'user_name' => \Models\LoginManager::getUserName(),
				'error_message' => $error_message,
				'user_id' => \Models\LoginManager::getUserId(),
				'templateList' => $templates,
				'page' => $page,
				'start' => $start,
				'pageCount' => $pageCount
			]);
		}

		$templates = $this->container['db']->table('m_template')->take(self::PAGE_SIZE)->get();
		$max = $this->container['db']->table('m_template')->get()->count();
		$pageCount = ceil($max / self::PAGE_SIZE);
		if ($pageCount == 0) $pageCount = 1;
		return $this->container->view->render($response, 'template.tpl', [
			'user_name' => \Models\LoginManager::getUserName(),
			'delete_message' => 'テンプレートを削除しました。',
			'user_id' => \Models\LoginManager::getUserId(),
			'templateList' => $templates,
			'page' => $page,
			'start' => $start,
			'pageCount' => $pageCount
		]);
	}
}