<?php

/**
 * Created by PhpStorm.
 * User: hiroaki
 * Date: 2017/06/12
 * Time: 13:32
 */
namespace Controllers;

use InvalidArgumentException;
use Psr\Container\ContainerInterface;

class ApiController {
	protected $container;
	private $areas = ['snake', 'water', 'america', 'africa', 'elephant', 'monkey', 'australia', 'asia', 'little', 'bear'];

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	private function convertDb2Response($question) {
		$imageUrl = [
			$question->image_url_1,
			$question->image_url_2
		];
		shuffle($imageUrl);
		return [
			'questionId' => $question->id,
			'title' => $question->title,
			'isCorrect' => $question->is_correct,
			'area' => $question->area,
			'imageUrl1' => $imageUrl[0],
			'imageUrl2' => $imageUrl[1],
		];
	}

	public function questions($request, $response, $args) {
		$parsedBody = $request->getParsedBody();
		$body = $response->getBody();

		@$level = $parsedBody['level'];

		if ($level == 'tobemaster') $isEasy = 0;
		else $isEasy = 1;

		$questions = [];
		try {
			$selectAreas = array_rand($this->areas, $isEasy === 1 ? 5 : 10);
			foreach ($selectAreas as $area) {
				$questions[] = $this->convertDb2Response(
					$this->container['db']
						->table('m_question')
						->where('is_easy', $isEasy)
						->where('area', $this->areas[$area])
						->get()
						->random(1)
						->first()
				);
			}
		} catch (InvalidArgumentException $e) {
			$questions = [
				"error" => "問題数が足りません"
			];
		}
		$responseArray = [
			'results' => $questions,
			'level' => $level
		];

		$body->write(json_encode($responseArray));
		return $response->withHeader(
			'Content-Type',
			'application/json'
		)->withBody($body);
	}

	public function results($request, $response, $args) {
		$parsedBody = $request->getParsedBody();
		$body = $response->getBody();

		@$answers = json_decode($parsedBody['answers'], true);
		@$level = $parsedBody['level'];

		$results = [];
		$correctCount = 0;
		foreach ($answers as $answer) {
			$question = $this->container['db']->table('m_question')->where('id', $answer['questionId'])->first();
			if (empty($question)) $isCorrect = false;
			else {
				$isCorrect = $question->is_correct == $answer['isYes'];
				if ($isCorrect) $correctCount++;
			}

			$results[] = [
				"questionId" => $answer['questionId'],
				"comment" => $question->comment,
				"isYes" => $answer['isYes'],
				"imageUrl1" => $question->image_url_1,
				"imageUrl2" => $question->image_url_2,
				"isCorrect" => $isCorrect
			];
		}

		$rank = $this->container['db']->table('m_rank')->where('id', $level)->first();
		if ($correctCount <= $rank->range_1) $rankName = $rank->name_1;
		else if ($correctCount <= $rank->range_2) $rankName = $rank->name_2;
		else if ($correctCount <= $rank->range_3) $rankName = $rank->name_3;
		else $rankName = $rank->name_4;

		$response_object = [
			"results" => $results,
			"correctCount" => $correctCount,
			"rank" => $rankName
		];

		$body->write(json_encode($response_object));
		return $response->withHeader(
			'Content-Type',
			'application/json'
		)->withBody($body);
	}

}