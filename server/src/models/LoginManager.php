<?php
/**
 * Created by PhpStorm.
 * User: hiroaki
 * Date: 2017/06/14
 * Time: 10:04
 */

namespace Models;


class LoginManager {
	
	public static function isLogin() {
		return (isset($_SESSION['isLogin']) && $_SESSION['isLogin']);
	}
	
	public static function set($userName, $userId) {
		$_SESSION['isLogin'] = true;
		$_SESSION['userName'] = $userName;
		$_SESSION['userId'] = $userId;
	}
	
	public static function getUserName() {
		return $_SESSION['userName'];
	}

	public static function getUserId() {
		return $_SESSION['userId'];
	}
	
	public static function delete() {
		unset($_SESSION["isLogin"]);
		unset($_SESSION["userName"]);
	}

}