<?php
/**
 * Created by PhpStorm.
 * User: hiroaki
 * Date: 2017/08/26
 * Time: 22:53
 */

namespace Models;


class User {
	public $name;

	public $user_id;
	
	public function __construct($id, $name) {
		$this->name = $name;
		$this->user_id = $id;
	}

}