<?php
// DIC configuration

$container = $app->getContainer();

// Register Smarty View helper
$container['view'] = function ($c) {
	$view = new \Slim\Views\Smarty(__DIR__ . "/../public/templates", [
		'compileDir' => __DIR__ . "/../templates_c"
	]);

	// Add Slim specific plugins
	$smartyPlugins = new \Slim\Views\SmartyPlugins($c['router'], $c['request']->getUri());
	$view->registerPlugin('function', 'path_for', [$smartyPlugins, 'pathFor']);
	$view->registerPlugin('function', 'base_url', [$smartyPlugins, 'baseUrl']);

	return $view;
};

$container['db'] = function ($c) {
	$capsule = new \Illuminate\Database\Capsule\Manager;
	$capsule->addConnection($c['settings']['db']);
	$capsule->setAsGlobal();
	$capsule->bootEloquent();
	return $capsule;
};

// monolog
$container['logger'] = function ($c) {
	$settings = $c->get('settings')['logger'];
	$logger = new Monolog\Logger($settings['name']);
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
	return $logger;
};
