<?php
return [
	'settings' => [
		'displayErrorDetails' => true, // set to false in production
		'addContentLengthHeader' => false, // Allow the web server to send the content-length header

		// Renderer settings
		'renderer' => [
			'template_path' => __DIR__ . '/../templates/',
		],

		'determineRouteBeforeAppMiddleware' => false,
		'displayErrorDetails' => true,

		'db' => [
			'driver' => 'mysql',
			'host' => '0.0.0.0',
			'database' => 'tobemaster',
			'username' => 'iw4c_user',
			'password' => 'iw4c_tobemaster',
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' => '',
		],

		// Monolog settings
		'logger' => [
			'name' => 'slim-app',
			'path' => __DIR__ . '/../logs/app.log',
			'level' => \Monolog\Logger::DEBUG,
		],
	],
];
