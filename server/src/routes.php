<?php

$app->group('/api', function() {
	$this->post('/questions', Controllers\ApiController::class.':questions');
	$this->post('/results', Controllers\ApiController::class.':results');
});

$app->group('/admin', function() {

	$this->group('', function () {
		$this->get('', Controllers\ManagementController::class.':index');
		$this->post('', Controllers\ManagementController::class.':login');
	})->add(function ($request, $response, $next) {
		if (Models\LoginManager::isLogin()) {
			return $response = $response->withStatus(302)->withHeader('Location', '/admin/questions');
		}
		return $next($request, $response);
	});

	$this->get('/logout', Controllers\ManagementController::class.':logout');


	$this->group('', function (){
		$this->group('/questions', function () {
			$this->get('', Controllers\ManagementController::class.':getQuestions');
			$this->get('/new', Controllers\ManagementController::class.':addQuestion');
			$this->post('', Controllers\ManagementController::class.':addQuestionExecute');
			$this->get('/{id}/edit', Controllers\ManagementController::class.':editQuestion');
			$this->put('/{id}', Controllers\ManagementController::class.':editQuestionExecute');
			$this->delete('/{id}', Controllers\ManagementController::class.':deleteQuestionExecute');
		});

		$this->group('/users', function () {
			$this->get('', Controllers\ManagementController::class.':getUsers');
			$this->get('/new', Controllers\ManagementController::class.':addUser');
			$this->post('', Controllers\ManagementController::class.':addUserExecute');
			$this->get('/{id}/edit', Controllers\ManagementController::class.':editUser');
			$this->put('/{id}', Controllers\ManagementController::class.':editUserExecute');
			$this->delete('/{id}', Controllers\ManagementController::class.':deleteUserExecute');
		});

		$this->group('/ranks', function () {
			$this->get('', Controllers\ManagementController::class.':getRanks');
			$this->post('', Controllers\ManagementController::class.':editRanksExecute');
		});

		$this->group('/templates', function () {
			$this->get('', Controllers\ManagementController::class.':getTemplates');
			$this->post('', Controllers\ManagementController::class.':addTemplateExecute');
			$this->put('/{id}', Controllers\ManagementController::class.':editTemplateExecute');
			$this->delete('/{id}', Controllers\ManagementController::class.':deleteTemplateExecute');
		});
	})->add(function ($request, $response, $next) {
		if (Models\LoginManager::isLogin()) {
			return $next($request, $response);
		}
		return $response = $response->withStatus(302)->withHeader('Location', '/admin');
	});

});

