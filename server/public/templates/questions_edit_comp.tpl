<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="/css/dropify.css">
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/dropify.js"></script>
<script>
	$(function () {
		$(".logout").click(function () {
			if (window.confirm('ログアウトしてもよろしいですか？')) {
				location.href = "/admin/logout";
			}
		});
		$("#template").change(function () {
			var val = $(this).val();
			$("#title").val(val);
		});
		$(function () {
			//画像ファイルプレビュー表示のイベント追加 fileを選択時に発火するイベントを登録
			$('form').on('change', 'input[type="file"]', function (e) {
				console.log($(this).parent);

				var file = e.target.files[0],
						reader = new FileReader(),
						$preview = $(this).parent().find(".preview");

				// 画像ファイル以外の場合は何もしない
				if (file.type.indexOf("image") < 0) {
					return false;
				}

				// ファイル読み込みが完了した際のイベント登録
				reader.onload = (function (file) {
					return function (e) {
						//既存のプレビューを削除
						$preview.empty();
						// .prevewの領域の中にロードした画像を表示するimageタグを追加
						$preview.append($('<img>').attr({
							src: e.target.result,
							width: "100%",
							class: "preview",
							title: file.name
						}));
					};
				})(file);

				reader.readAsDataURL(file);
			});
		});
	});

</script>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>問題編集</title>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/build/darkroom.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/page.css">
	<link href="/css/dropify.css" rel="stylesheet">
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/questions.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<form action="/admin/questions/{$id}" method="post" enctype="multipart/form-data">
	<input name="_METHOD" type="hidden" value="PUT">
	<input name="id" type="hidden" value="{$id}">
	<div class="wrapper">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header" style="width: 160px;">
						<span class="navbar-brand header_name">問題編集</span>
						<!-- <a class="navbar-brand">サイト名</a> -->
					</div>
					<p class="navbar-text navbar-right user_name">{$user_name}</p>
				</div>
				<div>
					<ul class="nav navbar-nav">
						<li><a href="/admin/users">ユーザー</a></li>
						<li class="active"><a href="/admin/questions">問題</a></li>
						<li><a href="/admin/ranks">ランク</a></li>
						<li><a href="/admin/templates">定型文</a></li>
					</ul>
					<p class="navbar-text navbar-right logout"><a>ログアウト</a></p>
				</div>
			</nav>
			{if !empty($error_message)}
				<div class="alert alert-danger">
					{foreach from=$error_message item=msg}
						<li style="list-style: none">{$msg}</li>
					{/foreach}
				</div>
			{/if}
			<div class="row">
				<div class="col-xs-8">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<select class="form-control" id="template" name="template">
									<option value="" color="gray">定型文を使用する場合はこちら</option>
									{foreach from=$templateList item="template" name="templateListLoop"}
										<option value="{$template->title}">{$template->title}</option>
									{/foreach}
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
                                <textarea name="title" id="title" rows="2"
										  style="font-size: 16px;" placeholder="問題文"
										  class="form-control ">{$title|default:''}</textarea>
							</div>
						</div>
					</div>
					<div class="row picture_change">
						<div class="col-xs-6 ">
							<input type="file" name="image1" class="dropify" id="image1"/>
							<div class="preview" style="margin-bottom: 32px">
								{if !empty($imageUrl1)}
									<img src="{$imageUrl1}">
									<input type="hidden" name="imageUrl1" value="{$imageUrl1}">
								{/if}
							</div>
						</div>
						<div class="col-xs-6">
							<input type="file" name="image2" class="dropify" id="image2"/>
							<div class="preview" style="margin-bottom: 32px">
								{if !empty($imageUrl2)}
									<img src="{$imageUrl2}">
									<input type="hidden" name="imageUrl2" value="{$imageUrl2}">
								{/if}
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<select class="form-control" id="area" name="area">
									<option value="">エリア</option>
									<option {if $area eq 'snake'}selected{/if} value="snake">スネークハウスストリート</option>
									<option {if $area eq 'water'}selected{/if} value="water">ウォーターストリート</option>
									<option {if $area eq 'america'}selected{/if} value="america">アメリカストリート</option>
									<option {if $area eq 'africa'}selected{/if} value="africa">アフリカストリート</option>
									<option {if $area eq 'elephant'}selected{/if} value="elephant">ゾウストリート</option>
									<option {if $area eq 'monkey'}selected{/if} value="monkey">モンキータウン</option>
									<option {if $area eq 'australia'}selected{/if} value="australia">オーストラリアストリート
									</option>
									<option {if $area eq 'asia'}selected{/if} value="asia">アジアストリート</option>
									<option {if $area eq 'little'}selected{/if} value="little">リトルワールド</option>
									<option {if $area eq 'bear'}selected{/if} value="bear">ベアストリート</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							解答
						</div>
						<div class="col-xs-4">
							<label><input type="radio" name="isCorrect" id="easy" value="1" {if $isCorrect|default:1 eq 1}checked{/if}>YES</label>
						</div>
						<div class="col-xs-4">
							<label><input type="radio" name="isCorrect" id="hard" value="0" {if $isCorrect|default:1 eq 0}checked{/if}>NO</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							難易度
						</div>
						<div class="col-xs-4">
							<label><input type="radio" name="isEasy" value="1" {if $isEasy|default:1 eq 1}checked{/if}>簡単</label>
						</div>
						<div class="col-xs-4">
							<label><input type="radio" name="isEasy" value="0" {if $isEasy|default:1 eq 0}checked{/if}>それ以外</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" id="comment">
							<div class="form-group">
                                        <textarea name="comment" id="comment" rows="10"
												  style="font-size: 16px;" placeholder="解説コメント"
												  class="form-control">{$comment|default:''}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<div class="col-xs-12">
								<input type="submit" class="btn btn-primary btn-block" value="編集">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="/vendor/fabric.js"></script>
	<script src="/build/darkroom.js"></script>
	<script src="/js/edit.js"></script>
</form>
</body>
</html>