<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="/css/dropify.css">
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/dropify.js"></script>
<script>
    $(function () {
        $(".logout").click(function () {
            if (window.confirm('ログアウトしてもよろしいですか？')) {
                location.href = "/admin/logout";
            }
        });
        $('.dropify').dropify();
        $("#easy").click(function () {
            $('#commentary').show();
        });
        $("#hard").click(function () {
            $('#commentary').hide();
        });
    });
</script>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザー編集</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/questions.css" rel="stylesheet">
    <link href="/css/users.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" style="width: 160px;">
                    <span class="navbar-brand header_name">ユーザー編集</span>
                    <!-- <a class="navbar-brand">サイト名</a> -->
                </div>
                <p class="navbar-text navbar-right user_name">{$user_name}</p>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/admin/users">ユーザー</a></li>
                    <li><a href="/admin/questions">問題</a></li>
                    <li><a href="/admin/ranks">ランク</a></li>
                    <li><a href="/admin/templates">定型文</a></li>
                </ul>
                <p class="navbar-text navbar-right logout"><a>ログアウト</a></p>
            </div>
        </nav>
        {if !empty($error_message)}
            <div class="alert alert-danger">
                {foreach from=$error_message item=msg}
                    <li style="list-style: none">{$msg}</li>
                {/foreach}
            </div>
        {/if}
        <form action="/admin/users/{$user->user_id}" method="post">
            <input type="hidden" name="_METHOD" value="PUT"/>
            <div class="row">
                <div class="form-group col-xs-2">
                    <p><b>ユーザーID</b></p>
                </div>
                <div class="form-group col-xs-10">
                    <label class="sr-only" for="id">ユーザーID</label>
                    <p>{$user->user_id}</p>
                    <input type="hidden" name="id" value="{$user->user_id}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-2">
                    <p><b>名前</b></p>
                </div>
                <div class="form-group col-xs-10">
                    <label class="sr-only" for="password">名前</label>
                    <input type="text" class="form-control" name="name" placeholder="名前" value="{$user->name}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-2">
                    <p><b>パスワード</b></p>
                </div>
                <div class="form-group col-xs-10">
                    <label class="sr-only" for="password">パスワード</label>
                    <input type="password" class="form-control" name="password" placeholder="パスワード" id="pass" value="">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xs-2">
                    <p><b>パスワード確認</b></p>
                </div>
                <div class="form-group col-xs-10">
                    <label class="sr-only" for="password">パスワード確認</label>
                    <input type="password" class="form-control" name="password_check" placeholder="パスワード確認" id="verifypass" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-success btn-block">編集</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
