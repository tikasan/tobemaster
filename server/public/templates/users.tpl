<!DOCTYPE html>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/dropify.js"></script>
<script>
    $(function () {
        $(".logout").click(function () {
            if (window.confirm('ログアウトしてもよろしいですか？')) {
                location.href = "/admin/logout";
            }
        });
        $('.dropify').dropify();
        $(".delete").click(function () {
            if (window.confirm('こちらの情報を削除してもよろしいですか？')) {
                var id = $(this).attr("id");
                location.href = "/admin/users/" + id;
            }
        });
    });
</script>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザー一覧</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/questions.css" rel="stylesheet">
    <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" style="width: 160px;">
                    <span class="navbar-brand header_name">ユーザー一覧</span>
                    <!-- <a class="navbar-brand">サイト名</a> -->
                </div>
                <p class="navbar-text navbar-right user_name">{$user_name}</p>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/admin/users">ユーザー</a></li>
                    <li><a href="/admin/questions">問題</a></li>
                    <li><a href="/admin/ranks">ランク</a></li>
                    <li><a href="/admin/templates">定型文</a></li>
                </ul>
                <p class="navbar-text navbar-right logout"><a>ログアウト</a></p>
            </div>
        </nav>
        {if !empty($delete_message)}
            <div class="alert alert-danger delete_message">{$delete_message}</div>
        {elseif !empty($edit_message)}
            <div class="alert alert-success edit_message">{$edit_message}</div>
        {elseif !empty($add_message)}
            <div class="alert alert-info add_message">{$add_message}</div>
        {elseif !empty($error_message)}
            <div class="alert alert-danger">
                {foreach from=$error_message item=msg}
                    <li style="list-style: none">{$msg}</li>
                {/foreach}
            </div>
        {/if}
        <div class="row">
            <div class="col-xs-3">
                <p><a href="/admin/users/new" class="btn btn-primary btn-block">ユーザーを追加する</a></p>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
            <tr>
                <th>名前</th>
                <th>ユーザーID</th>
                <th>編集</th>
                <th>削除</th>
            </tr>
            </thead>
            <tbody class="tbody">
            {foreach from=$userList item="user" name="userListLoop"}
                <tr>
                    <td>{$user->name}</td>
                    <td>{$user->user_id}</td>
                    <td>
                        <a href="/admin/users/{$user->user_id}/edit" class="btn btn-success edit">編集</a>
                    </td>
                    <td>
                        {if $user->user_id eq $user_id}
                            <button type="button" class="btn btn-danger delete disabled">削除</button>
                        {else}
                            <form action="/admin/users/{$user->user_id}" method="post">
                                <input name="_METHOD" type="hidden" value="DELETE">
                                <input type="submit" name="delete" class="btn btn-danger delete" id="{$user->user_id}"
                                       value="削除">
                            </form>
                        {/if}
                    </td>
                </tr>
                {foreachelse}
                <tr>
                    <td colspan="4">ユーザー情報は存在しません</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <div class="text-center">
            <ul class="pagination">
                {if $page neq 1}
                    <li><a href="/admin/users?page=1"><span>&laquo;</span></a></li>
                {/if}
                {for $i=$start to $pageCount max=5}
                    {if $i eq $page}
                        <li class="active"><span>{$i}</span></li>
                    {else}
                        <li><a href="/admin/users?page={$i}">{$i}</a></li>
                    {/if}
                {/for}
                {if $page neq $pageCount}
                    <li><a href="/admin/users?page={$pageCount}"><span>&raquo;</span></a></li>
                {/if}
            </ul>
        </div>
    </div>
</div>
</body>
</html>
