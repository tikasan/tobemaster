<!DOCTYPE html>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/dropify.js"></script>
<script>
    $(function () {
        var templateTableBodies = $('#templateTableBody tr');
        $.each(templateTableBodies, function (index, element) {
            $(element).find(".templateEdit").hide();
            $(element).find(".update").hide();
            $(element).find(".edit").on('click', function () {
                $(this).hide();
                $(element).find(".templateName").hide();
                $(element).find(".templateEdit").show();
                $(element).find(".update").show();
            });
        });

        $(".logout").click(function () {
            if (window.confirm('ログアウトしてもよろしいですか？')) {
                location.href = "/admin/logout";
            }
        });
    })
    ;
</script>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>定型文一覧</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/questions.css" rel="stylesheet">
    <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" style="width: 160px;">
                    <span class="navbar-brand header_name">定型文一覧</span>
                    <!-- <a class="navbar-brand">サイト名</a> -->
                </div>
                <p class="navbar-text navbar-right user_name">{$user_name}</p>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="/admin/users">ユーザー</a></li>
                    <li><a href="/admin/questions">問題</a></li>
                    <li><a href="/admin/ranks">ランク</a></li>
                    <li class="active"><a href="/admin/templates">定型文</a></li>
                </ul>
                <p class="navbar-text navbar-right logout"><a>ログアウト</a></p>
            </div>
        </nav>
        {if !empty($delete_message)}
            <div class="alert alert-danger delete_message">{$delete_message}</div>
        {elseif !empty($error_message)}
            <div class="alert alert-danger">
                {foreach from=$error_message item=msg}
                    <li style="list-style: none">{$msg}</li>
                {/foreach}
            </div>
        {elseif !empty($edit_message)}
            <div class="alert alert-success edit_message">{$edit_message}</div>
        {elseif !empty($add_message)}
            <p class="alert alert-info add_message">{$add_message}</p>
        {/if}
        <!-- <div class="alert alert-danger delete_message">情報を削除いたしました。</div> -->
        <div class="row" style="margin-bottom:20px">
            <form action="/admin/templates" method="post">
                <div class="col-xs-9">
                    <input type="text" name="title" class="form-control col-xs-9" placeholder="定型文を追加する">
                </div>
                <div class="col-xs-3">
                    <button type="submit" class="btn btn-primary btn-block col-xs-3">
                        追加
                    </button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                    <tr>
                        <th>定型文</th>
                        <th>編集</th>
                        <th>削除</th>
                    </tr>
                    </thead>
                    <tbody id="templateTableBody">
                    {foreach from=$templateList item="template" name="templateListLoop"}
                        <tr id="{$template->id}">
                            <td class="1 templateName">{$template->title}</td>

                            <form action="/admin/templates/{$template->id}" method="post">
                                <td class="templateEdit">
                                    <input type="text" name="title" value="{$template->title}" class="form-control title">
                                </td>
                                <td>
                                    <input name="_METHOD" type="hidden" value="PUT">
                                    <button type="button" name="edit" class="btn btn-success edit">編集</button>
                                    <input type="submit" name="update" class="btn btn-warning update" value="更新">
                                </td>
                            </form>

                            <td>
                                <form action="/admin/templates/{$template->id}" method="post">
                                    <input name="_METHOD" type="hidden" value="DELETE">
                                    <input type="submit" name="delete" class="btn btn-danger delete" value="削除">
                                </form>
                            </td>
                        </tr>
                        {foreachelse}
                        <tr>
                            <td colspan="4">定型文情報は存在しません</td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">
            <ul class="pagination">
                {if $page neq 1}
                    <li><a href="/admin/templates?page=1"><span>&laquo;</span></a></li>
                {/if}
                {for $i=$start to $pageCount max=5}
                    {if $i eq $page}
                        <li class="active"><span>{$i}</span></li>
                    {else}
                        <li><a href="/admin/templates?page={$i}">{$i}</a></li>
                    {/if}
                {/for}
                {if $page neq $pageCount}
                    <li><a href="/admin/templates?page={$pageCount}"><span>&raquo;</span></a></li>
                {/if}
            </ul>
        </div>
    </div>
</div>
</body>
</html>
