<!DOCTYPE html>
<script src="../js/jquery-3.2.1.min.js"></script>
<script>
    $(function () {
        var arg = new Object;
        var pair = location.search.substring(1).split('&');
        for (var i = 0; pair[i]; i++) {
            var kv = pair[i].split('=');
            arg[kv[0]] = kv[1];
        }
        $('#level-name').text(arg.level === 'high' ? '難しい' : "簡単");

        $(".logout").click(function () {
            if (window.confirm('ログアウトしてもよろしいですか？')) {
                location.href = "/admin/logout";
            }
        });
        $("#beginner").change(function () {
            var val = $(this).val();
            location.href = "/admin/ranks?level=" + val;
        });
    });
</script>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ランク編集</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/questions.css" rel="stylesheet">
    <link href="../css/ranks.css" rel="stylesheet">
    <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<form action="/admin/ranks" method="post">
    <div class="wrapper">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header" style="width: 160px;">
                        <span class="navbar-brand header_name">ランク編集</span>
                        <!-- <a class="navbar-brand">サイト名</a> -->
                    </div>
                    <p class="navbar-text navbar-right user_name">{$user_name}</p>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li><a href="/admin/users">ユーザー</a></li>
                        <li><a href="/admin/questions">問題</a></li>
                        <li class="active"><a href="/admin/ranks">ランク</a></li>
                        <li><a href="/admin/templates">定型文</a></li>
                    </ul>
                    <p class="navbar-text navbar-right logout"><a>ログアウト</a></p>
                </div>
            </nav>
            <div id="beginner_rank">
                <h1 class="page-header" id="level-name">簡単</h1>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group border">
                            <select class="form-control" id="beginner" name="level">
                                <option value="low" {if $level == 'low'}selected{/if} class="beginner">簡単</option>
                                <option value="high" {if $level == 'high'}selected{/if} class="tobemaster">難しい</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <div class="input-group ">
                        <span class="input-group-addon">～
                            <select name="range1">
                                {for $i=1;$i <= 10;$i++}
                                    {if $i == $ranks->range_1}
                                        <option value="{$i}" selected>{$i}</option>



                                                                                                                                                                        {else}



                                        <option value="{$i}">{$i}</option>
                                    {/if}
                                {/for}
                            </select>　点
                        </span>
                            <input type="text" class="form-control" name="user" value="{$ranks->name_1}"
                                   placeholder="ランク名" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <div class="input-group ">
                    <span class="input-group-addon">～
                        <select name="range2">
                            {for $i=1;$i <= 10;$i++}
                                {if $i == $ranks->range_2}
                                    <option value="{$i}" selected>{$i}</option>



                                                                                                                                                        {else}



                                    <option value="{$i}">{$i}</option>
                                {/if}
                            {/for}
                        </select>　点
                    </span>
                            <input type="text" class="form-control" name="user" value="{$ranks->name_2}"
                                   placeholder="ランク名" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <div class="input-group ">
                    <span class="input-group-addon">～
                        <select name="range3">
                            {for $i=1;$i <= 10;$i++}
                                {if $i == $ranks->range_3}
                                    <option value="{$i}" selected>{$i}</option>



                                                                                                                                                        {else}



                                    <option value="{$i}">{$i}</option>
                                {/if}
                            {/for}
                        </select>　点
                    </span>
                            <input type="text" class="form-control" name="user" value="{$ranks->name_3}"
                                   placeholder="ランク名" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <div class="input-group ">
                    <span class="input-group-addon">～
                        <select name="range4">
                            {for $i=1;$i <= 10;$i++}
                                {if $i == $ranks->range_4}
                                    <option value="{$i}" selected>{$i}</option>



                                                                                                                                                        {else}



                                    <option value="{$i}">{$i}</option>
                                {/if}
                            {/for}
                        </select>　点
                    </span>
                            <input type="text" class="form-control" name="user" value="{$ranks->name_4}"
                                   placeholder="ランク名" disabled="disabled">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-success btn-block">編集</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
</body>
</html>
