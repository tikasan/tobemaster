<!DOCTYPE html>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/dropify.js"></script>
<script>
    $(function () {
        $(".logout").click(function () {
            if (window.confirm('ログアウトしてもよろしいですか？')) {
                location.href = "/admin/logout";
            }
        });
        $('.dropify').dropify();
        $(".delete").click(function () {
            var id = $(this).attr("id");
            if (window.confirm('こちらの情報を削除してもよろしいですか?')) {
                location.href = "/questions/" + id;
            }
        });
        $('#level').change(function () {
            location.href = "/admin/questions?level=" + $(this).val();
        });
        $('#area').change(function () {
            location.href = "/admin/questions?area=" + $(this).val();
        });
    });
</script>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>問題一覧</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/questions.css" rel="stylesheet">
    <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" style="width: 160px;">
                    <span class="navbar-brand header_name">問題一覧</span>
                    <!-- <a class="navbar-brand">サイト名</a> -->
                </div>
                <p class="navbar-text navbar-right user_name">{$user_name}</p>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="/admin/users">ユーザー</a></li>
                    <li class="active"><a href="/admin/questions">問題</a></li>
                    <li><a href="/admin/ranks">ランク</a></li>
                    <li><a href="/admin/templates">定型文</a></li>
                </ul>
                <p class="navbar-text navbar-right logout"><a>ログアウト</a></p>
            </div>
        </nav>
        {if !empty($delete_message)}
            <div class="alert alert-danger delete_message">{$delete_message}</div>
        {elseif !empty($edit_message)}
            <div class="alert alert-success edit_message">{$edit_message}</div>
        {elseif !empty($add_message)}
            <div class="alert alert-info add_message">{$add_message}</div>
        {elseif !empty($error_message)}
            <div class="alert alert-danger">
                {foreach from=$error_message item=msg}
                    <li style="list-style: none">{$msg}</li>
                {/foreach}
            </div>
        {/if}
        <div class="row">
            <div class="col-xs-3">
                <form action="/admin/questions/new" method="GET">
                    <input type="submit" class="btn btn-primary" name="add" value="問題を追加する">
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <form action="/admin/questions" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="title" value="{$title|default:''}"
                                   placeholder="問題タイトル">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-3">
                <div class="form-group">
                    <select class="form-control" id="level" name="level">
                        <option value="">難易度</option>
                        <option value="1" {if $level eq 1}selected{/if}>簡単</option>
                        <option value="0" {if $level eq 0}selected{/if}>難しい</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <select class="form-control" id="area" name="area">
                        <option value="">エリア</option>
                        <option {if $area eq 'snake'}selected{/if} value="snake">スネークハウスストリート</option>
                        <option {if $area eq 'water'}selected{/if} value="water">ウォーターストリート</option>
                        <option {if $area eq 'america'}selected{/if} value="america">アメリカストリート</option>
                        <option {if $area eq 'africa'}selected{/if} value="africa">アフリカストリート</option>
                        <option {if $area eq 'elephant'}selected{/if} value="elephant">ゾウストリート</option>
                        <option {if $area eq 'monkey'}selected{/if} value="monkey">モンキータウン</option>
                        <option {if $area eq 'australia'}selected{/if} value="australia">オーストラリアストリート</option>
                        <option {if $area eq 'asia'}selected{/if} value="asia">アジアストリート</option>
                        <option {if $area eq 'little'}selected{/if} value="little">リトルワールド</option>
                        <option {if $area eq 'bear'}selected{/if} value="bear">ベアストリート</option>
                    </select>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
            <tr>
                <th>問題タイトル</th>
                <th>難易度</th>
                <th>エリア</th>
                <th>写真</th>
                <th>編集</th>
                <th>削除</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$questionsList item="question" name="questionsListLoop"}
                <tr id="table1">
                    <td>{$question->title}</td>
                    <td>{if $question->is_easy}簡単{else}難しい{/if}</td>
                    <td id="area">
                        {if $question->area eq 'snake'}
                            スネークハウスストリート
                        {elseif $question->area eq 'water'}
                            ウォーターストリート
                        {elseif $question->area eq 'america'}
                            アメリカストリート
                        {elseif $question->area eq 'africa'}
                            アフリカストリート
                        {elseif $question->area eq 'elephant'}
                            ゾウストリート
                        {elseif $question->area eq 'monkey'}
                            モンキータウン
                        {elseif $question->area eq 'australia'}
                            オーストラリアストリート
                        {elseif $question->area eq 'asia'}
                            アジアストリート
                        {elseif $question->area eq 'little'}
                            リトルワールド
                        {elseif $question->area eq 'bear'}
                            ベアストリート
                        {/if}
                    </td>
                    <td>
                        <img src="{$question->image_url_1}" alt="piece1" class="picture">
                        <img src="{$question->image_url_2}" alt="piece2" class="picture">
                    </td>
                    <td>
                        <form action="/admin/questions/{$question->id}/edit" method="GET">
                            <button type="submit" class="btn btn-success edit" id="edit">編集</button>
                        </form>
                    </td>
                    <td>
                        <form action="/admin/questions/{$question->id}" method="post">
                            <input name="_METHOD" type="hidden" value="delete">
                            <input type="submit" class="btn btn-danger delete" id="{$question->id}"
                                   name="delete" value="削除">
                        </form>
                    </td>
                </tr>
                {foreachelse}
                <tr>
                    <td colspan="6">問題情報は存在しません</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <div class="text-center">
            <ul class="pagination">
                {if $page neq 1}
                    <li><a href="/admin/questions?page=1"><span>&laquo;</span></a></li>
                {/if}
                {for $i=$start to $pageCount max=5}
                    {if $i eq $page}
                        <li class="active"><span>{$i}</span></li>
                    {else}
                        <li><a href="/admin/questions?page={$i}">{$i}</a></li>
                    {/if}
                {/for}
                {if $page neq $pageCount}
                    <li><a href="/admin/questions?page={$pageCount}"><span>&raquo;</span></a></li>
                {/if}
            </ul>
        </div>
    </div>
</div>
</body>
</html>
