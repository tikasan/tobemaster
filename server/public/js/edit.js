$(document).on('click',"#imgedit",function(){
	$("#file").change(function () {
		$('#target').removeAttr('src');
		var file = $(this).val();
		console.log($(this));
		var regex = /\\|\\/;
		var patharray = file.split(regex);
		var file = patharray[patharray.length -1];
		$("#target").attr("src","img/"+file);
		$("#trim").show();
		$("#imgedit").hide();
	});
});
$(document).on('click',"#trim",function(){
	var imagepath ="#image1 .dropify-wrapper .dropify-preview .dropify-render img";
	/*var imagepath = "#exbit1";*/
	var trimming =".trimming1";
	var base64 = "#base1";
	darkroom("#target",imagepath,trimming,base64);//関数呼び出し
	$("#trim").hide();
});
$(document).on('click',"#imgedit2",function(){
	$("#file2").change(function () {
		$('#target2').removeAttr('src');
		var file = $(this).val();
		console.log(file);
		console.log($(this));
		var regex = /\\|\\/;
		var patharray = file.split(regex);
		var file = patharray[patharray.length -1];
		$("#target2").attr("src","img/"+file);
		$("#trim2").show();
		$("#imgedit2").hide();
	});
});
$(document).on('click',"#trim2",function(){
	var imagepath ="#image2 .dropify-wrapper .dropify-preview .dropify-render img";
	var trimming =".trimming2";
	var base64 = "#base2";
	darkroom("#target2",imagepath,trimming,base64);//関数呼び出し
	("#trim2").hide();
});		
function darkroom(target,imagepath,trimming,base64){
	var dkrm = new Darkroom(target, {
				      // Size options
				      minWidth: 690,
				      minHeight: 450,
				      maxWidth:828,
				      maxHeight:540,
				      ratio: 4/3,
				      backgroundColor: '#000',
					      // Plugins options
					      plugins: {
					        //save: false,
					        save:{
					        	callback: function(e) {
					        		this.darkroom.selfDestroy(); // Turn off the bar and cleanup
					        		var newImage = dkrm.canvas.toDataURL();
					        		var ThatStoresYourImageData = newImage;
					 				$("#introduction").val(ThatStoresYourImageData);//言われたところ
					 				$("#base64").val(ThatStoresYourImageData);
					 				$(imagepath).attr('src',ThatStoresYourImageData);
					 				$(base64).val(ThatStoresYourImageData);
					 				base64ToImage(ThatStoresYourImageData, function(img) {
					 					document.getElementById('insertImage');
					 				});
					 				$(".picture_change").show();
					 				$(trimming).hide();
					 			}
					 		},
					 		crop: {
					          quickCropKey: 67, //key "c"
					          minHeight:300,
					          minWidth:460,
					          //ratio: 4/3
					      }
					  },
// Post initialize script
initialize: function() {
	var cropPlugin = this.plugins['crop'];
	cropPlugin.selectZone(0,0,460,300);/*最初の位置、横の位置.横幅サイズ、縦幅サイズ*/
//cropPlugin.requireFocus();//これは新しく生成するから消す
}

})
};
function base64ToImage(base64Img, callback) {
	var img = new Image();
	img.onload = function() {
		callback(img);
	};
	img.src = base64Img;
};				