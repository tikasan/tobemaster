<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="wrapper">

    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">

        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title text-center">ログイン</div>
            </div>
            <div class="panel-body">

                <!-- エラー表示-->
                <div class="alert alert-danger">
                    <b>ユーザーID又はパスワードが違います</b>
                </div> 
                <form action="users/users.tpl" name="form" id="form" class="form-horizontal" enctype="multipart/form-data" method="POST">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="user" type="text" class="form-control" name="userid" value="" placeholder="User">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password"
                               placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 controls">
                            <button type="submit" href="" class="btn btn-primary btn-block">ログイン</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
