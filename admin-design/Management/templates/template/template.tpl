<!DOCTYPE html>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/dropify.js"></script>
<script>
    $(function(){
        $('.delete_message').hide();
        hide();
        $('.dropify').dropify();
        $(".delete").click(function(){
            if(window.confirm('こちらの情報を削除してもよろしいですか？')){
                location.href = "users_delete.tpl";
            }
        });
        $(".logout").click(function(){
            if(window.confirm('ログアウトしてもよろしいですか？')){
                location.href = "../login.tpl";
            }
        });
        /*編集ボタンが押された時の処理*/
        $(document).on('click',"#one",function() {
            var id = $('#one').attr('id');
            update(id);
        });
        $(document).on('click',"#two",function() {
         var id = $('#two').attr('id');
         update(id);
     });
        $(document).on('click',"#three",function() {
         var id = $('#three').attr('id');
         update(id);
     });
        $(document).on('click',"#four",function() {
         var id = $('#four').attr('id');
         update(id);
     });
        $(document).on('click',"#five",function() {
         var id = $('#five').attr('id');
         update(id);
     });
        /*更新ボタンが押された時の処理*/
        $(document).on('click',"#one-update",function(){
            var id = $("#one-update").attr('id');
            edit(id);
        });
        $(document).on('click',"#two-update",function(){
            var id = $("#two-update").attr('id');
            edit(id);
        });
        $(document).on('click',"#three-update",function(){
            var id = $("#three-update").attr('id');
            edit(id);
        });
        $(document).on('click',"#four-update",function(){
            var id = $("#four-update").attr('id');
            edit(id);
        });
        $(document).on('click',"#five-update",function(){
            var id = $("#five-update").attr('id');
            edit(id);
        });
        /*編集から更新ボタン*/
        function update(id){//編集から更新ボタン
           var text_id =  "#"+id;
           var text_font = text_id + "-text";
           var text_box = text_id + "-box";
           var update_text = text_id +"-update-text";
           var text = $(text_font).text();//入力されているテキストの中身取得
           $(text_font).text("");//テキストを空白にする
           $(text_font).hide();//テキストの枠を隠す
           $(text_box).show();//テキストボックスの枠を表示
           $(update_text).val(text);//テキストボックスにテキストの中身を代入
           $(text_id).hide();//編集ボタンを隠す
           $("#"+id+"-update").show();//更新ボタンを表示
       }
       /*更新から編集ボタン*/
       function edit(id){//更新から編集ボタン
            var value = $("#"+id+"-text").val();//テキストボックスの中身取得
            $("#"+id).hide();//更新ボタン削除
            id=change(id);//ここでidが変換される
            var text_id =  "#"+id;
            var text_font = text_id + "-text";
            var text_box = text_id + "-box";
            $(text_font).show();
            $(text_box).hide();
            $(text_id).show();
            $("#update-text").hide();
            $(text_id+"-text").append(value);//テキスト追加
        }
        /*idの変換*/
        function change(id){
            if (id === "one-update") {
                id="one";
                return id;
            }
            else if(id === "two-update"){
                id="two";
                return id;
            }
            else if(id === "three-update"){
                id="three";
                return id;
            }
            else if(id === "four-update"){
                id="four";
                return id;
            }
            else if(id === "five-update"){
                id="five";
                return id;
            }
        }
        function hide(){
            $("#one-update").hide();
            $("#two-update").hide();
            $("#three-update").hide();
            $("#four-update").hide();
            $("#five-update").hide();
            $("#one-box").hide();
            $("#two-box").hide();
            $("#three-box").hide();
            $("#four-box").hide();
            $("#five-box").hide();
        }
    });
</script>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>定型文一覧</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/problems.css" rel="stylesheet">
  <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
      </head>
      <body>
        <div class="wrapper">
            <div class="container">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header" style="width: 160px;">
                            <span class="navbar-brand header_name">定型文一覧</span>
                            <!-- <a class="navbar-brand">サイト名</a> -->
                        </div>
                        <p class="navbar-text navbar-right user_name">HAL太郎</p>
                    </div>
                    <div>
                       <ul class="nav navbar-nav">
                        <li><a href="../users/users.tpl">ユーザー</a></li>
                        <li><a href="../question/questions.tpl">問題</a></li>
                        <li><a href="../rank/ranks.tpl">ランク</a></li>
                        <li class="active"><a href="template.tpl">定型文</a></li>
                    </ul>
                    <p class="navbar-text navbar-right logout"><a href="#">ログアウト</a></p>
                    </div>
                </nav>
                <!-- <div class="alert alert-danger delete_message">情報を削除いたしました。</div> -->
                <div class="row" style="margin-bottom:20px;"">
                    <div class="col-xs-9">
                        <input type="text" class="form-control col-xs-9" placeholder="定型文を追加する" >
                    </div>
                    <div class="col-xs-3">  
                        <form action="template.tpl" method="post">
                            <button class="btn btn-primary btn-block col-xs-3">
                                追加
                            </button>
                        </form>
                    </div>
                </div> 
                <div class="row">  
                    <div class="col-xs-12">      
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <thead>
                                <tr><th>定型文</th><th>編集</th><th>削除</th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="one-text">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</td>
                                    <td id="one-box">
                                        <input type="text" id="one-update-text" class="form-control">
                                    </td>
                                    <td id="one-control">
                                        <button class="btn btn-success edit" id="one">編集</button>
                                        <button class="btn btn-warning update" id="one-update">更新</button>
                                    </td>
                                    <td>
                                        <form action="" method="post">
                                            <button class="btn btn-danger delete" >削除</button>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="two-text">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</td>
                                    <td id="two-box">
                                        <input type="text" id="two-update-text" class="form-control">
                                    </td>
                                    <td id="two-control">
                                        <button class="btn btn-success edit" id="two">編集</button>
                                        <button class="btn btn-warning update" id="two-update">更新</button>
                                    </td>
                                    <td>
                                        <form action="" method="post">
                                            <button class="btn btn-danger delete" >削除</button>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="three-text">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</td>
                                   <td id="three-box">
                                    <input type="text" id="three-update-text" class="form-control">
                                </td>
                                <td id="three-control">
                                    <button class="btn btn-success edit" id="three">編集</button>
                                    <button class="btn btn-warning update" id="three-update">更新</button>
                                </td>
                                <td>
                                    <form action="" method="post">
                                        <button class="btn btn-danger delete" >削除</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td id="four-text">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</td>
                                <td id="four-box">
                                    <input type="text" id="four-update-text" class="form-control">
                                </td>
                                <td id="four-control">  
                                    <button class="btn btn-success edit" id="four">編集</button>
                                    <button class="btn btn-warning update" id="four-update">更新</button>                            
                                </td>
                                <td>
                                    <form action="" method="post">
                                        <button class="btn btn-danger delete" >削除</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td id="five-text">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</td>
                                <td id="five-box">
                                    <input type="text" id="five-update-text" class="form-control">
                                </td>
                                <td id="five-control">
                                    <button class="btn btn-success edit" id="five">編集</button>
                                    <button class="btn btn-warning update" id="five-update">更新</button>
                                </td>
                                <td>
                                    <form action="" method="post">
                                        <button class="btn btn-danger delete" >削除</button>
                                    </form>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="text-center">
                <ul class="pagination">
                    <li class="disabled"><span>&laquo;</span></li>
                    <li class="active"><a href="#"><span>1</span></a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><span>&raquo;</span></li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
