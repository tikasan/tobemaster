<!DOCTYPE html>
<script src="../js/jquery-3.2.1.min.js"></script>
<script>
    $(function () {
        $('#tobemaster_rank').hide();
        $(".logout").click(function(){
            if(window.confirm('ログアウトしてもよろしいですか？')){
                location.href = "../login.tpl";
            }
        });
        $("#beginner").change(function () {
            //選択したvalue値を変数に格納
            var val = $(this).val();
            rankChange(val);
        });
        $("#tobemaster").change(function () {
            //選択したvalue値を変数に格納
            var val = $(this).val();
            rankChange(val);
        });
        function rankChange(val) {
            if (val === "beginner") {
                $('#beginner_rank').show();
                $('#tobemaster_rank').hide();
                $('select[name="beginner"]').prop('selectedIndex',0);
            }
            else if (val === "tobemaster") {
                $('#beginner_rank').hide();
                $('#tobemaster_rank').show();
                $('select[name="tobemaster"]').prop('selectedIndex',1);
            }
        }
    });
</script>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ランク編集</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/problems.css" rel="stylesheet">
    <link href="../css/ranks.css" rel="stylesheet">
    <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header" style="width: 160px;">
                        <span class="navbar-brand header_name">ランク編集</span>
                        <!-- <a class="navbar-brand">サイト名</a> -->
                    </div>
                    <p class="navbar-text navbar-right user_name">HAL太郎</p>
                </div>
                <div>
                 <ul class="nav navbar-nav">
                    <li><a href="../users/users.tpl">ユーザー</a></li>
                    <li><a href="../question/questions.tpl">問題</a></li>
                    <li class="active"><a href="ranks.tpl">ランク</a></li>
                    <li><a href="../template/template.tpl">定型文</a></li>
                </ul>
                <p class="navbar-text navbar-right logout"><a href="#">ログアウト</a></p>
            </div>
        </nav>
        <div id="beginner_rank">
            <h1 class="page-header">簡単</h1>
            <div class="row">
             <div class="col-xs-12">
                <div class="form-group border">
                    <select class="form-control" id="beginner" name="beginner">
                        <option value="beginner" selected="selected" class="beginner">簡単</option>
                        <option value= "tobemaster" class="tobemaster">難しい</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-xs-12">
                <div class="input-group ">
                    <span class="input-group-addon">～
                        <select name="range1">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>　点
                    </span>
                    <input type="text" class="form-control" name="user" value="初心者" placeholder="ランク名" disabled="disabled">
                </div>
            </div>


            <div class="form-group col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">～
                        <select name="range2">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>　点
                    </span>
                    <input type="text" class="form-control" name="" value="リピーター" placeholder="ランク名" disabled="disabled">
                </div>
            </div>

            <div class="form-group col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">～
                        <select name="range3">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>　点
                    </span>
                    <input type="text" class="form-control" name="user" value="飼育員" placeholder="ランク名" disabled="disabled">
                </div>
            </div>

            <div class="form-group col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">～
                        <select name="range4">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>　点
                    </span>
                    <input type="text" class="form-control" name="user" value="園長" placeholder="ランク名" disabled="disabled">
                </div>
            </div>
        </div>
    </div>
    <div id="tobemaster_rank">
        <h1 class="page-header">難しい</h1>
        <div class="row">
         <div class="col-xs-12">
            <div class="form-group border">
                <select class="form-control" id="tobemaster" name="tobemaster">
                    <option value="beginner" class="beginner">簡単</option>
                    <option value="tobemaster" selected="selected" class="tobemaster">難しい</option>
                </select>
            </div>
        </div>
        <div class="form-group col-xs-12">
            <div class="input-group ">
                <span class="input-group-addon">～
                    <select name="range1">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>　点
                </span>
                <input type="text" class="form-control" name="user" value="リピーター" placeholder="ランク名" disabled="disabled">
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="form-group col-xs-12">
            <div class="input-group">
                <span class="input-group-addon">～
                    <select name="range2">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>　点
                </span>
                <input type="text" class="form-control" name="user" value="飼育員" placeholder="ランク名" disabled="disabled">
            </div>
        </div>
    </div>



    <div class="row">
        <div class="form-group col-xs-12">
            <div class="input-group">
                <span class="input-group-addon">～
                    <select name="range3">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>　点
                </span>
                <input type="text" class="form-control" name="user" value="園長" placeholder="ランク名" disabled="disabled">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="form-group col-xs-12">
            <div class="input-group">
                <span class="input-group-addon">～
                    <select name="range4">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>　点
                </span>
                <input type="text" class="form-control" name="user" value="とべマスター" placeholder="ランク名" disabled="disabled">
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-xs-12">
        <a href="ranks.tpl" class="btn btn-success btn-block">編集</a>
    </div>
</div>

</div>
</body>
</html>
