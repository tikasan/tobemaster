<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="../css/dropify.css">
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/dropify.js"></script>
<script>
	$(function() {
		$(".logout").click(function(){
            if(window.confirm('ログアウトしてもよろしいですか？')){
                location.href = "../login.tpl";
            }
        });
		$('.dropify').dropify();
		$("#easy").click(function() {
			$('#comment').show();
		});
		$("#hard").click(function() {
			$('#comment').hide();
		});
		$("#exbit1").click(function () {
			$('#q_image1').hide();
		});
		$("#exbit2").click(function () {
			$('#q_image2').hide();
		});
		$("#sample").change(function () {
			var val = $(this).val();
			$("#questions").val(val);
		});
	});
jQuery.event.add(window, "load", function(){
        var sl = '.trimming img'; //リサイズしたい画像のクラス名
        var fw = 350;        // サムネイルの幅
        var fh = 350;        // サムネイルの高さ
        var iw, ih;
        $(sl).each(function(){
            var w = $(this).width(); // 画像の幅(原寸)
            var h = $(this).height(); // 画像の高さ(原寸)

            //横長の画像の場合
            if (w >= h) {
                iw = (fh/h*w-fw)/2
                $(this).height(fh); //高さをサムネイルに合わせる
                $(this).css("top",0);
                $(this).css("left","-"+iw+"px");//画像のセンター合わせ
            } 

            //縦長の画像の場合
            else {
                ih = (fw/w*h-fh)/2
                $(this).width(fw); //幅をサムネイルに合わせる
                $(this).css("top","-"+ih+"px");//画像のセンター合わせ
                $(this).css("left",0);
            }
        });
    });
</script>
<style>
div.trimming {
  width:350px;
  height:350px;
  overflow:hidden;
  float: left;
  }

.trimming img { position:relative;}

.cle { clear: both;} /*float解除用*/
</style>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>問題編集</title>
	<link href="../css/dropify.css" rel="stylesheet">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/problems.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header" style="width: 160px;">
						<span class="navbar-brand header_name">問題編集</span>
						<!-- <a class="navbar-brand">サイト名</a> -->
					</div>
					<p class="navbar-text navbar-right user_name">HAL太郎</p>
				</div>
				<div>
					<ul class="nav navbar-nav">
						<li><a href="../users/users.tpl">ユーザー</a></li>
						<li class="active"><a href="questions.tpl">問題</a></li>
						<li><a href="../rank/ranks.tpl">ランク</a></li>
						<li><a href="../template/template.tpl">定型文</a></li>
					</ul>
					<p class="navbar-text navbar-right logout"><a href="#">ログアウト</a></p>
				</div>
			</nav>
			<div class="row">
				<div class="col-xs-8">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<select class="form-control" id="sample" name="sample" >
									<option value="" color="gray">定型文を使用する場合はこちら</option>
									<option value="大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</option>
									<option value="大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。">大量の餌の写真と動物並べて。１日に餌を食べてる量が〇kgである。</option>
									<option value="大量の餌の。１日に餌を食べてる量が〇kgである。">大量の餌の。１日に餌を食べてる量が〇kgである。</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<textarea name="title" id="title" rows="2"
								style="font-size: 16px;" placeholder="問題文"
								class="form-control ">この二つの動物は年齢が同じか？</textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<p class="image_edit">
								<div class="trimming"><input type="file" name="exbit2" class="dropify" id="exbit2" data-default-file="../img/piece2.jpg"//></div>
							</p>
						</div>
						<div class="col-xs-6">
							<p class="image_edit">
								<input type="file" name="exbit2" class="dropify" id="exbit2" data-default-file="../img/piece2.jpg"//>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<select class="form-control" id="area" name="area">
										<option value="asia">アジアストリート</option>
										<option value="africa">アフリカストリート</option>
										<option value="america">アメリカストリート</option>
										<option value="water" selected="selected">ウォーターストリート</option>
										<option value="australia">オーストラリアストリート</option>
										<option value="snake">スネークハウスストリート</option>
										<option value="elephant">ゾウストリート</option>
										<option value="bear">ベアストリート</option>
										<option value="monkey">モンキータウン</option>
										<option value="little">リトルワールド</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								回答
							</div>
							<div class="col-xs-4">
								<label><input type="radio" name="isEasy" id="easy"
									checked>YES</label>
								</div>
								<div class="col-xs-4">
									<label><input type="radio" name="isEasy" id="hard">NO</label>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									難易度
								</div>
								<div class="col-xs-4">
									<label><input type="radio" name="chk" checked>簡単</label>
								</div>
								<div class="col-xs-4">
									<label><input type="radio" name="chk">それ以外</label>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12" id="comment">
									<div class="form-group">
										<textarea name="comment_w" id="comment_w" rows="12"
										style="font-size: 16px;" placeholder="解説コメント"
										class="form-control"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<div class="col-xs-12">
									<button class="btn btn-success btn-block">編集</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>
