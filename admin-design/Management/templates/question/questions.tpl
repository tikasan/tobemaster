<!DOCTYPE html>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/dropify.js"></script>
<script>
	$(function() {
		$(".logout").click(function(){
            if(window.confirm('ログアウトしてもよろしいですか？')){
                location.href = "../login.tpl";
            }
        });
		$('.delete_message').hide();
		$('.dropify').dropify();
		$(".delete").click(function() {
			if (window.confirm('こちらの情報を削除してもよろしいですか?')) {
				location.href = "questions_delete.tpl";
			}
		});
	});
</script>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>問題一覧</title>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/problems.css" rel="stylesheet">
	<!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header" style="width: 160px;">
                            <span class="navbar-brand header_name">問題一覧</span>
                            <!-- <a class="navbar-brand">サイト名</a> -->
                        </div>
                        <p class="navbar-text navbar-right user_name">HAL太郎</p>
                    </div>
                    <div>
                       <ul class="nav navbar-nav">
						<li><a href="../users/users.tpl">ユーザー</a></li>
						<li class="active"><a href="questions.tpl">問題</a></li>
						<li><a href="../rank/ranks.tpl">ランク</a></li>
						<li><a href="../template/template.tpl">定型文</a></li>
					</ul>
                        <p class="navbar-text navbar-right logout"><a href="#">ログアウト</a></p>
                    </div>
                </nav>
			<div class="row">
				<div class="col-xs-3">
					<p>
						<a href="questions_add.tpl" class="btn btn-primary btn-block">問題を追加する</a>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="問題タイトル">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">
						<select class="form-control" id="area" name="">
							<option value="アジア">難易度</option>
						</select>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">
						<select class="form-control" id="area" name="area">
							<option value="アジア">エリア</option>
						</select>
					</div>
				</div>
			</div>
			<table class="table table-striped table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>問題タイトル</th>
						<th>難易度</th>
						<th>エリア</th>
						<th>写真</th>
						<th>編集</th>
						<th>削除</th>
					</tr>
				</thead>
				<tbody>
					<tr id="table1">
						<td id="title">この二つの動物は年齢が同じか？</td>
						<td id="easy">簡単</td>
						<td id="area">ベアストリート</td>
						<td><img src="../img/piece1.jpg" alt="piece1" class="picture"><img
							src="../img/piece2.jpg" alt="piece1" class="picture"></td>
							<td>
								<form action="questions_edit_comp.tpl" method="post">
									<button class="btn btn-success edit" data-toggle="popover" id="edit">編集</button>
								</form>
							</td>
							<td>
								<form action="" method="post">
									<button class="btn btn-danger delete">削除</button>
								</form>
							</td>
						</tr>
						<tr>
							<td>この二つの動物は年齢が同じか？</td>
							<td>簡単</td>
							<td>ベアストリート</td>
							<td><img src="../img/piece1.jpg" alt="piece1" class="picture"><img
								src="../img/piece2.jpg" alt="piece1" class="picture"></td>
								<td>
									<form action="questions_edit_comp.tpl" method="post">
										<button class="btn btn-success edit">編集</button>
									</form>
								</td>
								<td>
									<form action="" method="post">
										<button class="btn btn-danger delete">削除</button>
									</form>
								</td>
							</tr>
							<tr>
								<td>この二つの動物は年齢が同じか？</td>
								<td>簡単</td>
								<td>ベアストリート</td>
								<td><img src="../img/piece1.jpg" alt="piece1" class="picture"><img
									src="../img/piece2.jpg" alt="piece1" class="picture"></td>
									<td>
										<form action="questions_edit_comp.tpl" method="post">
											<button class="btn btn-success edit">編集</button>
										</form>
									</td>
									<td>
										<form action="" method="post">
											<button class="btn btn-danger delete">削除</button>
										</form>
									</td>
								</tr>
								<tr>
									<td>この二つの動物は年齢が同じか？</td>
									<td>簡単</td>
									<td>ベアストリート</td>
									<td><img src="../img/piece1.jpg" alt="piece1" class="picture"><img
										src="../img/piece2.jpg" alt="piece1" class="picture"></td>
										<td>
											<form action="questions_edit_comp.tpl" method="post">
												<button class="btn btn-success edit">編集</button>
											</form>
										</td>
										<td>
											<form action="" method="post">
												<button class="btn btn-danger delete">削除</button>
											</form>
										</td>
									</tr>
									<tr>
										<td>この二つの動物は年齢が同じか？</td>
										<td>簡単</td>
										<td>ベアストリート</td>
										<td><img src="../img/piece1.jpg" alt="piece1" class="picture"><img
											src="../img/piece2.jpg" alt="piece1" class="picture"></td>
											<td>
												<form action="questions_edit_comp.tpl" method="post">
													<button class="btn btn-success edit">編集</button>
												</form>
											</td>
											<td>
												<form action="" method="post">
													<button class="btn btn-danger delete">削除</button>
												</form>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="text-center">
									<ul class="pagination">
										<li class="disabled"><span>&laquo;</span></li>
										<li class="active"><a href="#"><span>1</span></a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li><span>&raquo;</span></li>
									</ul>
								</div>
							</div>
						</div>
					</body>
					</html>
