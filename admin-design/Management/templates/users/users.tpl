<!DOCTYPE html>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/dropify.js"></script>
<script>
    $(function(){
        $(".logout").click(function(){
            if(window.confirm('ログアウトしてもよろしいですか？')){
                location.href = "../login.tpl";
            }
        });
        $('.delete_message').hide();
        $('.dropify').dropify();
        $(".delete").click(function(){
            if(window.confirm('こちらの情報を削除してもよろしいですか？')){
                location.href = "users_delete.tpl";
            }
        });
    });
</script>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザー一覧</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/problems.css" rel="stylesheet">
  <!-- <link href="css/problems_collection.css" rel="stylesheet"> -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
      </head>
      <body>
        <div class="wrapper">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header" style="width: 160px;">
                            <span class="navbar-brand header_name">ユーザー一覧</span>
                            <!-- <a class="navbar-brand">サイト名</a> -->
                        </div>
                        <p class="navbar-text navbar-right user_name">HAL太郎{$user-name}</p>
                    </div>
                    <div>
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="users.tpl">ユーザー</a></li>
                            <li><a href="../question/questions.tpl">問題</a></li>
                            <li><a href="../rank/ranks.tpl">ランク</a></li>
                            <li><a href="../template/template.tpl">定型文</a></li>
                        </ul>
                        <p class="navbar-text navbar-right logout"><a href="#">ログアウト</a></p>
                    </div>
                </nav>

                <div class="alert alert-danger delete_message">情報を削除いたしました。</div>

                <div class="row">
                    <div class="col-xs-3">
                        <p><a href="users_add.tpl" class="btn btn-primary btn-block">ユーザーを追加する</a></p>
                    </div>
                </div>           
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                        <tr><th>名前</th><th>ユーザーID</th><th>編集</th><th>削除</th></tr>
                    </thead>
                    <tbody class="tbody">
                        {foreach from=$userList item="user" name="userListLoop"}
                        <tr>
                            <td>HAL太郎{$user-name}</td>
                            <td>1111111{$user-userid}</td>
                            <td>
                                <form action="users_edit.tpl" method="post">
                                    <button class="btn btn-success edit">編集</button>
                                </form>
                            </td>
                            <td>
                                <form action="" method="post">
                                    <button class="btn btn-danger delete" >削除</button>
                                </form>
                            </td>
                        </tr>
                        {foreachelse}
                        <tr>
                        <td colspan="4">ユーザー情報は存在しません</td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
                <div class="text-center">
                    <ul class="pagination">
                        <li class="disabled"><span>&laquo;</span></li>
                        <li class="active"><a href="#"><span>1</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><span>&raquo;</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
    </html>
