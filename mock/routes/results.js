const express = require('express');
const router = express.Router();

router.get('/', function(req, res, next) {
  res.json({
    results: [
      {
        questionId: 1,
        comment: '解説1',
        isYes: true,
        imageUrl1: 'http://hoge11.jpg',
        imageUrl2: 'http://hoge12.jpg',
        isCorrect: true,
      }, {
        questionId: 2,
        comment: '解説2',
        isYes: true,
        imageUrl1: 'http://hoge21.jpg',
        imageUrl2: 'http://hoge22.jpg',
        isCorrect: true,
      }, {
        questionId: 3,
        comment: '解説3',
        isYes: true,
        imageUrl1: 'http://hoge11.jpg',
        imageUrl2: 'http://hoge12.jpg',
        isCorrect: true,
      }, {
        questionId: 4,
        comment: '解説4',
        isYes: true,
        imageUrl1: 'http://hoge21.jpg',
        imageUrl2: 'http://hoge22.jpg',
        isCorrect: true,
      }, {
        questionId: 5,
        comment: '解説5',
        isYes: true,
        imageUrl1: 'http://hoge11.jpg',
        imageUrl2: 'http://hoge12.jpg',
        isCorrect: true,
      },
    ],
    correctCount: 2,
    rank: '飼育員',
    level: 'トベマスター',
  });
});

module.exports = router;
