const express = require('express');
const router = express.Router();

router.get('/', function(req, res, next) {
  res.json({
    results: [
      {
        questionId: 1,
        title: '問題ですよ1',
        imageUrl1: '../assets/images/questions/sample1.png',
        imageUrl2: '../assets/images/questions/sample2.jpg',
        isYes: true,
      }, {
        questionId: 2,
        title: '問題ですよ2',
        imageUrl1: '../assets/images/questions/sample1.png',
        imageUrl2: '../assets/images/questions/sample2.jpg',
        isYes: false,
      },{
        questionId: 3,
        title: '問題ですよ3',
        imageUrl1: '../assets/images/questions/sample1.png',
        imageUrl2: '../assets/images/questions/sample2.jpg',
        isYes: true,
      }, {
        questionId: 4,
        title: '問題ですよ4',
        imageUrl1: '../assets/images/questions/sample1.png',
        imageUrl2: '../assets/images/questions/sample2.jpg',
        isYes: false,
      },{
        questionId: 5,
        title: '問題ですよ5',
        imageUrl1: '../assets/images/questions/sample1.png',
        imageUrl2: '../assets/images/questions/sample2.jpg',
        isYes: true,
      },
    ],
    level: 'beginner',
  });
});

module.exports = router;
