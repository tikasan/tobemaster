/* @flow */
import Vue from 'vue';
import Router from 'vue-router';
import TobeApp from '@/components/TobeApp';
import LevelSelection from '@/components/LevelSelection';
import Question from '@/components/Question';
import Result from '@/components/Result';
import Explanation from '@/components/Explanation';
import Reward from '@/components/Reward';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'tobeapp',
      component: TobeApp,
      props: true
    },
    {
      path: '/level-selection',
      name: 'level-selection',
      component: LevelSelection,
      props: true
    },
    {
      path: '/question',
      name: 'question',
      component: Question,
      props: true
    },
    {
      path: '/question:id',
      component: Question,
      props: true
    },
    {
      path: '/explanation',
      name: 'explanation',
      component: Explanation,
      props: true
    },
    {
      path: '/result',
      name: 'result',
      component: Result,
      props: true
    },
    {
      path: '/reward',
      name: 'reward',
      component: Reward,
      props: true
    }
  ]
});
