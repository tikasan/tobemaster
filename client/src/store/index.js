/* @flow */
import 'whatwg-fetch';

const state = {
  setup: {
    level: ''
  },
  questions: {},
  error: [],
  questionStackId: 0,
  question: {},
  answers: {
    answers: [],
    level: ''
  },
  results: {}
};

const actions = {
  debug: process.env.NODE_ENV !== 'production',
  pushLevel: (selectLevel) => {
    if (selectLevel) {
      state.setup.level = selectLevel;
    } else {
      this.debug && console.log('selectLevel is not asigned');
    }
  },
  pickQuestion: () => {
    state.question = {};
    state.question = state.questions.results[state.questionStackId];
  },
  nextquestionStackId: () => {
    state.questionStackId += 1;
  },
  getQuestions: fetch('http://localhost:3000/questions')
  .then(response => response.json())
  .then(json => {
    this.debug && console.log('triggered getQuestions');
    return {
      level: json['level'],
      results: json['results']
    };
  })
  .catch(e => {
    state.error.push('parsing failed', e);
  }),
  setQuestions: (getQuestions) => {
    state.questions = getQuestions;
  },
  setAnswers: (questionId, isYes) => {
    switch (isYes) {
      case 'no':
        state.answers.answers.push({
          questionId: questionId,
          isYes: false
        });
        break;

      case 'yes':
        state.answers.answers.push({
          questionId: questionId,
          isYes: true
        });
        break;

      default:
        break;
    }
  },
  getResults: fetch('http://localhost:3000/results')
  .then(response => response.json())
  .then(json => {
    this.debug && console.log('triggered getResults');
    return {
      level: json['level'],
      rank: json['rank'],
      correctCount: json['correctCount'],
      results: json['results']
    };
  })
  .catch(e => {
    state.error.push('parsing failed', e);
  }),
  setResults: (getResults) => {
    state.results = getResults;
  },
};

export {state, actions};
