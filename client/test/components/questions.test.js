/* @flow */
require('whatwg-fetch');
const state = {
  questions: {},
  error: [],
  questionStackId: 0,
  question: {},
  answers: {
    answers: [],
    level: '',
  },
};

const actions = {
  debug: process.env.NODE_ENV !== 'production',
  getQuestions: fetch('http://localhost:3000/questions').
  then(response => response.json()).
  then(json => {
    this.debug && console.log('triggered getQuestions');
    return {
      level: json['level'],
      results: json['results'],
    };
  }).
  catch(e => {
    state.error.push('parsing failed', e);
  }),
  setQuestions: (getQuestions) => {
    state.questions = getQuestions;
  },
};

test.skip('actions', async () => {
  expect.assertions(1);
  actions.setQuestions(await actions.getQuestions);
  expect(state.questions).toEqual({
    'level': 'beginner',
    'results': [
      {
        'imageUrl1': '../assets/images/questions/sample1.png',
        'imageUrl2': '../assets/images/questions/sample2.jpg',
        'isYes': true,
        'questionId': 1,
        'title': '問題ですよ1',
      },
      {
        'imageUrl1': '../assets/images/questions/sample1.png',
        'imageUrl2': '../assets/images/questions/sample2.jpg',
        'isYes': false,
        'questionId': 2,
        'title': '問題ですよ2',
      },
    ],
  });
});

test.skip('actions', async () => {
  expect.assertions(1);
  actions.setQuestions(await actions.getQuestions);
  expect(state.questions.results[0]).toEqual({
    'imageUrl1': '../assets/images/questions/sample1.png',
    'imageUrl2': '../assets/images/questions/sample2.jpg',
    'isYes': true,
    'questionId': 1,
    'title': '問題ですよ1',
  });
});

test('actions', async () => {
  expect.assertions(1);
  actions.setQuestions(await actions.getQuestions);
  expect(state.questions.results[0].title).toEqual('問題ですよ1');
});
