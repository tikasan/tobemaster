/* @flow */
require('whatwg-fetch');
const state = {
  results: {},
  error: [],
};

const actions = {
  debug: process.env.NODE_ENV !== 'production',
  getResults: fetch('http://localhost:3000/results').
  then(response => response.json()).
  then(json => {
    this.debug && console.log('triggered getResults');
    return {
      level: json['level'],
      rank: json['rank'],
      correctCount: json['correctCount'],
      results: json['results'],
    };
  }).
  catch(e => {
    state.error.push('parsing failed', e);
  }),
  setResults: (getResults) => {
    state.results = getResults;
  },
};

test.skip('actions', async () => {
  expect.assertions(1);
  await actions.setResults(await actions.getResults);
  expect(state.results).toEqual({
    "correctCount": 2,
    "level": "トベマスター",
    "rank": "飼育員",
    "results": [
      {
        "comment": "解説1",
        "imageUrl1": "http://hoge11.jpg",
        "imageUrl2": "http://hoge12.jpg",
        "isCorrect": true,
        "isYes": true,
        "questionId": 1
      },
      {
        "comment": "解説2",
        "imageUrl1": "http://hoge21.jpg",
        "imageUrl2": "http://hoge22.jpg",
        "isCorrect": true,
        "isYes": true,
        "questionId": 2
      }
    ]
  });
});

test('actions', async () => {
  expect.assertions(1);
  await actions.setResults(await actions.getResults);
  expect(state.results.results[0].comment).toEqual();
});
