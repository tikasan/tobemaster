/* @flow */
const state = {
  setup: {
    area: '',
    level: '',
  },
};

const actions = {
  debug: process.env.NODE_ENV !== 'production',
  pushArea: (selectArea) => {
    state.setup.area = selectArea;
  },
  pushLevel: (selectLevel) => {
    state.setup.level = selectLevel;
  },
};

test('state.setup.area', async () => {
  expect.assertions(1);
  await actions.pushArea('snake');
  expect(state.setup.area).toEqual('snake');
});

test('state.setup.level', async () => {
  expect.assertions(1);
  await actions.pushLevel('beginner');
  expect(state.setup.level).toEqual('beginner');
});
