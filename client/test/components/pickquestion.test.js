/* @flow */
const state = {
  setup: {
    level: '',
  },
  questions: {
    'results': [
      {
        'questionId': 1,
        'title': '問題ですよ1',
        'imageUrl1': '../assets/images/questions/sample1.png',
        'imageUrl2': '../assets/images/questions/sample2.jpg',
        'isYes': true,
      },
      {
        'questionId': 2,
        'title': '問題ですよ2',
        'imageUrl1': '../assets/images/questions/sample1.png',
        'imageUrl2': '../assets/images/questions/sample2.jpg',
        'isYes': false,
      },
    ],
    'level': 'beginner',
  },
  error: [],
  questionStackId: 0,
  question: {},
  answers: {
    answers: [],
    level: '',
  },
  results: {},
};

const actions = {
  debug: process.env.NODE_ENV !== 'production',
  pickQuestion: () => {
    state.question = {};
    state.question = state.questions.results[2];
  },
};

test('actions', async () => {
  expect.assertions(1);
  await actions.pickQuestion();
  expect(state.question).toEqual('');
});
